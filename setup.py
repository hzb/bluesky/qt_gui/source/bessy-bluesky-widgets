import sys
from os import path

from setuptools import find_packages, setup

import versioneer

setup(name='bessyii_bluesky_widgets',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='A collection of bluesky widgets that can be used at BESSY II',
      url='https://gitlab.helmholtz-berlin.de/bessyii/bluesky/bessyii_bluesky_widgets',
      author='Huling He, Sebastian Sachse, Will Smith',
      author_email='william.smith@helmholtz-berlin.de',
      # license='MIT',
      packages=find_packages(),
      install_requires=[
          'numpy',
          'python-icat',
          'bluesky-widgets',
          'QtPy',
          'pyqt5',
          'bluesky-queueserver',
      ]
      # zip_safe=False
)
