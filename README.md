# bessyii_bluesky_widgets

A collection of qt widgets for use in bluesky guis at BESSY II

## Dynamic Plan Editor Using Plan Annotation

To use the full functionality of the plan creator widget you can use [plan annotation](https://blueskyproject.io/bluesky-queueserver/plan_annotation.html) in your ipython environment. You can describe any plan arguments like that. A simple example for the count plan could look like this:

```python
from bluesky.plans import count

def new_count(detectors, num:int, delay:float=0.1, *, md:dict):
    yield from count(detectors, num, delay)
```

By adding types to the arguments the widget can now understand what is supposed to be passed to the plan and creates a widget based on that information. The default value is also passed and used in the widget. This is the simplest way to use this functionality but it can be extended using a decorator. For example:

```python
from bluesky.plans import count
from bluesky_queueserver import parameter_annotation_decorator

@parameter_annotation_decorator({
    "parameters": {
        "num": {
            "annotation": "int",
            "default": 1,
            "min": 1,
            "max": 20,
            "step": 1,
        },
        "delay": {
            "annotation": "float",
            "default": 0.1,
            "min": 0.0,
            "max": 1.0,
            "step": 0.2,
        }
    }
})
def new_count(detectors, num, delay, *, md:dict):
    yield from count(detectors, num, delay)
```

In the above example the parameter detector is described as a list of strings that the environment expects to be initiated devices. For the numeric fields you can pass things like default and min/max that are also used by the widget to set these details for the numeric input.


| Annotation Type  |      Description   | Additional Fields | Example |
|----------|-------------|------|----|
| int |  python Built-in Type integer | default<br>min<br>max<br>step | [example](#integer-example) |
| float | python Built-in Type float | default<br>min<br>max<br>step | [example](#float-example) |
| bool | python Built-in Type boolean | default | [example](#boolean-example) |
| typing.List[str] | List of strings. The widget will asume this is used for devices. | convert_device_names | [example](#list-of-string-example) |
| typing.List[typing.Tuple[str, float, float]] | List of tuple. The widget will asume this is used for movables like motors. You can extend the tuple by any fields needed. | convert_device_names | [example](#list-of-tuple-example) |
| typing.List[\_\_PLAN\_\_] | List of plans. This expects a list of plans in the namespace of the environment. |  | [example](#list-of-plan-example) |
| enums | enumerator - special syntax, see example below.<br> The annotation name has to be the same as the string inside the enums field.  | |  [example](#enumerator-example) |

### Integer Example
```python
@parameter_annotation_decorator({
    "parameters": {
        "an_int": {
            "annotation": "int",
            "default": 10,
            "min": 1,
            "max": 20,
            "step": 1,
        },
    }
})

```

### Float Example
```python
@parameter_annotation_decorator({
    "parameters": {
        "a_float": {
            "annotation": "float",
            "default": 10.0,
            "min": 0.0,
            "max": 20.0,
            "step": 0.2,
        }
    }
})

```

### Boolean Example
```python
@parameter_annotation_decorator({
    "parameters": {
        "a_bool": {
            "annotation": "bool",
            "default": True,
        },
    }
})

```

### List of String Example
```python
@parameter_annotation_decorator({
    "parameters": {
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
    }
})

```

### List of Tuple Example
```python
@parameter_annotation_decorator({
    "parameters": {
        "motors": {
            "description": "motor, start, stop, num",
            "annotation": "typing.List[typing.Tuple[str, float, float, int]]",
            "convert_device_names": True,
        },
    }
})

```

### List of Plan Example
```python
@parameter_annotation_decorator({
    "parameters": {
        "plans": {
            "annotation": "typing.List[__PLAN__]"
        },
    }
})

```

### Enumerator Example
```python
@parameter_annotation_decorator({
    "parameters": {
        "an_enum": {
            "annotation": "An_Enum",
            "enums": {"An_Enum": ["", "1", "3","5","7","Max Flux", "Auto"]},
        },
})

```

 

![Example](docs/plan_annotation.png)

## Queue Control Buttons

There are two buttons for controlling the queue. They change depending on the state of the queue. 

After pressing play you will be able to pause the current running plan by pressing either the pause or stop button. 

A subsequent press of the stop button will stop the queue and move the currently paused plan back to the queue. 

![queue_control](queue_control.png)

You can also skip to the next plan in the queue and run it by pressing the "skip" button. 

Hovering over all buttons will give a tooltip about what they do. 

## Batches

It is possible to save batches of plans which are a list saved as a CSV file as json strings. 

The files are saved by default at the location specified by the environment variable: `BATCH_SAVE_DIR`

To create a batch:

1. Add some plans to the queue using the plan editor
2. Select one or more plans
3. Press the Star icon above the queue widget
4. Give the batch a name and save it

To restore a named batch:

1. Press the file icon called "Load Batch to Queue"
2. Select the batch you want to load back the queue. 

![Batches](docs/batches.png)

## Progress Bar 

Assuming the GUI is started before the RE starts a scan and the RE is emitting documents over a zmq buffer, then a progress bar will be displayed if plans include the following in their metadata dict:

`plan_args`: A dict of the plan arguments. This will be displayed at the top of the GUI 
`num_points`: The number of events in the plan.





