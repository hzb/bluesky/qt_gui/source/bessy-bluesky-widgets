from qtpy.QtWidgets import (
    QWidget,
    QGroupBox,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QTableWidget,
    QTableWidgetItem,
    QTableView,
    QHeaderView,
    QAbstractItemView,
    QTextEdit,
    QTabWidget,
    QRadioButton,
    QButtonGroup,
    QComboBox,
    QLineEdit,
    QCheckBox,
    QDialog,
    QFileDialog,
    QDialogButtonBox,
)

from bluesky_widgets.qt.run_engine_client import (
    QueueTableWidget,
    PushButtonMinimumWidth,
)
from qtpy.QtCore import Qt, Signal, Slot, QTimer, QSize
from qtpy.QtGui import QFontMetrics, QPalette, QBrush, QColor, QIntValidator, QIcon
import copy
import os
import time


class QtHistoryControl(QWidget):
    signal_update_widgets = Signal()
    signal_update_selection = Signal(object)
    signal_plan_history_changed = Signal(object, object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self._monitor_mode = False

        # Set True to block processing of table selection change events
        self._block_table_selection_processing = False

        self._table_column_labels = (
            "",
            "Name",
            "STATUS",
            "Parameters",
            "USER",
            "GROUP",
        )
        self._table = QueueTableWidget()
        self._table.setColumnCount(len(self._table_column_labels))
        # self._table.verticalHeader().hide()
        self._table.setHorizontalHeaderLabels(self._table_column_labels)
        self._table.horizontalHeader().setSectionsMovable(True)

        self._table.setVerticalScrollMode(QAbstractItemView.ScrollPerItem)
        self._table.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)

        self._table.setSelectionBehavior(QTableView.SelectRows)
        self._table.setSelectionMode(QTableWidget.ContiguousSelection)
        self._table.setShowGrid(True)
        self._table.setAlternatingRowColors(True)

        # Prevents horizontal autoscrolling when clicking on an item (column) that
        # doesn't fit horizontally the displayed view of the table (annoying behavior)
        self._table.setAutoScroll(False)

        self._table.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        self._table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self._table.horizontalHeader().setStretchLastSection(True)
        self._table.horizontalHeader().setMinimumSectionSize(5)

        self._table_scrolled_to_bottom = False
        # self._table_slider_is_pressed = False

        # The following parameters are used only to control widget state (e.g. activate/deactivate
        #   buttons), not to perform real operations.
        self._n_table_items = 0  # The number of items in the table
        self._selected_items_pos = []  # Selected items (table rows)

        ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
        self._clear_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/clear.png'))
        self._copy_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/copy.png'))
        self._edit_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/edit.png'))

        self._clear_btn = QPushButton()
        self._clear_btn.setIcon(self._clear_icon)
        self._clear_btn.setToolTip('Clear History')

        self._copy_btn = QPushButton()
        self._copy_btn.setIcon(self._copy_icon)
        self._copy_btn.setToolTip('Copy to Queue')

        self._edit_btn = QPushButton()
        self._edit_btn.setIcon(self._edit_icon)
        self._edit_btn.setToolTip('Edit Item')

        self._copy_btn.clicked.connect(self._pb_copy_to_queue_clicked)
        self._clear_btn.clicked.connect(self._pb_clear_history_clicked)
        self._edit_btn.clicked.connect(self._pb_edit_clicked)

        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("HISTORY"))
        hbox.addStretch(1)
        hbox.addWidget(self._copy_btn)
        hbox.addWidget(self._edit_btn)
        hbox.addStretch(2)
        hbox.addWidget(self._clear_btn)
        vbox.addLayout(hbox)
        vbox.addWidget(self._table)
        self.setLayout(vbox)

        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widgets.connect(self.slot_update_widgets)

        self.model.events.plan_history_changed.connect(self.on_plan_history_changed)
        self.signal_plan_history_changed.connect(self.slot_plan_history_changed)

        self.model.events.history_item_selection_changed.connect(self.on_history_item_selection_changed)
        self.signal_update_selection.connect(self.slot_change_selection)

        self._table.itemSelectionChanged.connect(self.on_item_selection_changed)
        self._table.verticalScrollBar().valueChanged.connect(self.on_vertical_scrollbar_value_changed)
        self._table.verticalScrollBar().rangeChanged.connect(self.on_vertical_scrollbar_range_changed)
        self._table.cellDoubleClicked.connect(self._on_table_cell_double_clicked)

        self._update_button_states()

    @property
    def monitor_mode(self):
        return self._monitor_mode

    @monitor_mode.setter
    def monitor_mode(self, monitor):
        self._monitor_mode = bool(monitor)
        self._update_button_states()

    def on_vertical_scrollbar_value_changed(self, value):
        max = self._table.verticalScrollBar().maximum()
        self._table_scrolled_to_bottom = value == max

    def on_vertical_scrollbar_range_changed(self, min, max):
        if self._table_scrolled_to_bottom:
            self._table.verticalScrollBar().setValue(max)

    def on_update_widgets(self, event):
        self.signal_update_widgets.emit()

    @Slot()
    def slot_update_widgets(self):
        self._update_button_states()

    def _update_button_states(self):
        is_connected = bool(self.model.re_manager_connected)
        n_items = self._n_table_items
        n_selected_items = self._selected_items_pos
        mon = self._monitor_mode

        is_sel = bool(n_selected_items)
        one_sel = len(n_selected_items) == 1

        self._copy_btn.setEnabled(is_connected and not mon and is_sel)
        self._edit_btn.setEnabled(is_connected and not mon and is_sel and one_sel)
        self._clear_btn.setEnabled(is_connected and not mon and n_items)

    def on_plan_history_changed(self, event):
        plan_history_items = event.plan_history_items
        selected_item_pos = event.selected_item_pos
        self.signal_plan_history_changed.emit(plan_history_items, selected_item_pos)

    @Slot(object, object)
    def slot_plan_history_changed(self, plan_history_items, selected_item_pos):
        # Check if the vertical scroll bar is scrolled to the bottom.
        scroll_value = self._table.verticalScrollBar().value()
        scroll_maximum = self._table.verticalScrollBar().maximum()
        self._table_scrolled_to_bottom = scroll_value == scroll_maximum

        self._table.clearContents()
        self._table.setRowCount(len(plan_history_items))

        if len(plan_history_items):
            resize_mode = QHeaderView.ResizeToContents
        else:
            # Empty table, stretch the header
            resize_mode = QHeaderView.Stretch
        self._table.horizontalHeader().setSectionResizeMode(resize_mode)

        for nr, item in enumerate(plan_history_items):
            for nc, col_name in enumerate(self._table_column_labels):
                try:
                    value = self.model.get_item_value_for_label(item=item, label=col_name)
                except KeyError:
                    value = ""
                table_item = QTableWidgetItem(value)
                table_item.setFlags(table_item.flags() & ~Qt.ItemIsEditable)
                self._table.setItem(nr, nc, table_item)

        # Update the number of table items
        self._n_table_items = len(plan_history_items)

        # Advance scrollbar if the table is scrolled all the way down.
        if self._table_scrolled_to_bottom:
            scroll_maximum_new = self._table.verticalScrollBar().maximum()
            self._table.verticalScrollBar().setValue(scroll_maximum_new)

        # Call function directly
        self.slot_change_selection(selected_item_pos)

        self._update_button_states()

    def on_item_selection_changed(self):
        """
        The handler for ``item_selection_changed`` signal emitted by QTableWidget
        """
        if self._block_table_selection_processing:
            return

        sel_rows = self._table.selectionModel().selectedRows()
        try:
            if len(sel_rows) >= 1:
                selected_item_pos = [_.row() for _ in sel_rows]
                self.model.selected_history_item_pos = selected_item_pos
                self._selected_items_pos = selected_item_pos
            else:
                raise Exception()
        except Exception:
            self.model.selected_history_item_pos = []
            self._selected_items_pos = []

    def on_history_item_selection_changed(self, event):
        """
        The handler for the event generated by the model
        """
        row = event.selected_item_pos
        self.signal_update_selection.emit(row)

    def _on_table_cell_double_clicked(self, n_row, n_col):
        """
        Double-clicking of an item of the table widget: send the item (plan) for processing.
        """
        self.model.history_item_send_to_processing()

    @Slot(object)
    def slot_change_selection(self, selected_item_pos):
        rows = selected_item_pos

        # Keep horizontal scroll value while the selection is changed (more consistent behavior)
        scroll_value = self._table.horizontalScrollBar().value()

        if not rows:
            self._table.clearSelection()
            self._selected_items_pos = []
        else:
            self._block_table_selection_processing = True
            self._table.clearSelection()
            for row in rows:
                for col in range(self._table.columnCount()):
                    item = self._table.item(row, col)
                    if item:
                        item.setSelected(True)
                    else:
                        print(f"Plan Queue Table: attempting to select non-existing item: row={row} col={col}")

            if self._table.currentRow() not in rows:
                self._table.setCurrentCell(rows[-1], 0)

            row_visible = rows[-1]
            item_visible = self._table.item(row_visible, 0)
            self._table.scrollToItem(item_visible, QAbstractItemView.EnsureVisible)
            self._block_table_selection_processing = False
            self._selected_items_pos = rows

        self._table.horizontalScrollBar().setValue(scroll_value)

        self.model.selected_history_item_pos = selected_item_pos
        self._update_button_states()

    def _pb_copy_to_queue_clicked(self):
        try:
            self.model.history_item_add_to_queue()
        except Exception as ex:
            print(f"Exception: {ex}")

    def _pb_deselect_all_clicked(self):
        self._table.clearSelection()
        self._selected_items_pos = []
        self._update_button_states()

    def _pb_clear_history_clicked(self):
        try:
            self.model.history_clear()
        except Exception as ex:
            print(f"Exception: {ex}")

    def _pb_edit_clicked(self):
        sel_items = self.model.selected_history_item_pos
        if len(sel_items) == 1:
            sel_item_uid = sel_items[0]
            sel_item = copy.deepcopy(self.model._plan_history_items[sel_item_uid])# Returns deep copy
            self.model.events.queue_item_edit(item=sel_item)