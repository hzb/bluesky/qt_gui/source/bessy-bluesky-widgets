from qtpy.QtWidgets import (
    QWidget,
    QLabel,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QFileDialog,
    QLineEdit,
    QGroupBox,
    QGridLayout,
    QDialog,
    QListWidget,
    QFrame,
    QMenu,
    QScrollArea,
    QDialog,
    QTableWidget,
    QTableWidgetItem,
    QAbstractScrollArea,
)

from bluesky_widgets.qt.search import QtSearch

from .input import (
    QtExportButton,
)

from .baseline_search import QtBaselineDialog

import numpy as np

from qtpy.QtCore import Qt

class QtSearchBase(QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def contextMenuEvent(self, event):
        contextMenu = QMenu(self)

        catalog = self.model.selection_as_catalog
        menus = {}

        def addMenu(parent, children, title):
            menu = QMenu(parent)
            menu.setTitle(title)
            if isinstance(children, dict):
                for key in children.keys():
                    if isinstance(children[key], dict) or isinstance(children[key], list):
                        menu.addMenu(addMenu(menu, children[key], key))
                    else:
                        sub = QMenu(menu)
                        sub.setTitle(str(key))
                        sub.addAction(str(children[key]))
                        menu.addMenu(sub)
            elif isinstance(children, list):
                for item in children:
                    if isinstance(item, dict) or isinstance(item, list):
                        menu.addMenu(addMenu(menu, item, str(item)))
                    else:
                        menu.addAction(str(item))
            return menu

        #for scanID in catalog.keys():
        #    contextMenu.addMenu(addMenu(contextMenu, catalog[scanID].metadata, scanID))

        baseline_action = contextMenu.addAction('Show Baseline')
            
        action = contextMenu.exec_(self.mapToGlobal(event.pos()))
        if action == baseline_action:
            datasets = dict()
            for uid, run in catalog.items(): 
                try:
                    run.baseline
                    datasets[str(uid)] = run
                except AttributeError as e:
                    print(f"No Baseline for scan ID: {run.metadata['start']['scan_id']}")
            if len(datasets) > 0:   
                viewer = QtBaselineDialog(datasets = datasets)

        elif action:
            print(action.text())

global QtSearch

bases = QtSearchBase.mro().copy()
bases.insert(0, QtSearch)

class_dict = {}
#make sure the override works corretly
for base in reversed(bases):
    class_dict.update(base.__dict__)

QtSearch = type('QtSearch', tuple(bases), class_dict)

class QtSearchWithButton(QWidget):
    """
    A view for SearchWithButton.

    Combines the QtSearch widget with a button that processes the selected Runs.
    """

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        layout = QVBoxLayout()
        self.setLayout(layout)
        layout.addWidget(QtSearch(model))

        go_button = QPushButton("View Selected Runs")
        layout.addWidget(go_button)
        go_button.clicked.connect(self._on_go_button_clicked)

        #export_button = QtExportButton(name='Export Selected Run', model=self.model, callback=self._export_button_clicked)
        #layout.addWidget(export_button)

    def _on_go_button_clicked(self):
        events = self.model.events
        if events is not None:
            events.view()
    
    #def _export_button_clicked(self, path, run):
    #    nexus_writer = NXWriterBESSY()
    #    nexus_writer.file_path = path
    #    nexus_writer.export_run(run)


class QtBaselineDialog(QDialog):
    def __init__(self, *args, datasets, **kwargs):
        super().__init__(*args, **kwargs)
        self.datasets = datasets
        self.setWindowTitle('Dataviewer')
        self.setWindowModality(Qt.ApplicationModal)

        box = QVBoxLayout()
        self.viewer = QtBaselineViewer(datasets = datasets)
        box.addWidget(self.viewer)

        self.setLayout(box)
        self.exec_()


class QtBaselineViewer(QWidget):
    def __init__(self, *args, datasets, **kwargs):
        super().__init__(*args, **kwargs)
        self.datasets = datasets

        box = QVBoxLayout()
        self.table = QTableWidget()
        self.table.setMinimumHeight(800)
        self.table.setMinimumWidth(600)
        self.table.horizontalHeader().setSectionsMovable(True)
        self.table.horizontalHeader().setStyleSheet("font:11pt")
        self.table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        box.addWidget(self.table)

        self.populate()

        self.setLayout(box)

    def populate(self):
        list_of_dets = []
        self.table.setColumnCount(len(self.datasets.keys()) + 1)
        baseline = {}
        for uid in self.datasets.keys():
            scanid = self.datasets[uid].metadata['start']['scan_id']
            baseline[scanid] = self.datasets[uid].baseline.read()
            baseline[scanid]['UID'] = uid
            for key in baseline[scanid].keys():
                if not key in list_of_dets:
                    list_of_dets.append(key)
        self.table.setRowCount(len(list_of_dets))
        
        for i in range(len(list_of_dets)):
            self.table.setItem(i, 0, QTableWidgetItem(list_of_dets[i]))
            col = 1
            for sid in baseline.keys():
                if list_of_dets[i] in baseline[sid].keys():
                    try:
                        val = baseline[sid][list_of_dets[i]].values[0]
                    except IndexError as e:
                        val = baseline[sid][list_of_dets[i]].values
                    self.table.setItem(i, col, QTableWidgetItem(str(val)))
                col += 1
        self.table.setHorizontalHeaderLabels([''] + [str(x) for x in baseline.keys()])
        self.table.resizeColumnsToContents()