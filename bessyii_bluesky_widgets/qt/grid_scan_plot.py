from bluesky_widgets.qt.figures import QtFigure
from bluesky_widgets.models.plot_builders import RasteredImages
from qtpy.QtWidgets import (
    QWidget,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QComboBox,
    QLabel,
    QTabWidget,
)

class QtAddImagePlot(QWidget):
    """
    This is the widget to get the RasteredImages result of grid scan from the databroker.
    """
    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        self.totalLayout = QVBoxLayout()
        self.layout = QHBoxLayout()
        self.totalLayout.addLayout(self.layout)
        self.imageLayout = QVBoxLayout()
        self.totalLayout.addLayout(self.imageLayout)
        self.result = QTabWidget(tabsClosable=True)
        self.result.tabCloseRequested.connect(self.onTabCloseRequested)
        self.setLayout(self.totalLayout)

        self.y_selector = QComboBox(self)
        self.y_selector.setEditable(True)
        self.y_selector.resize(400,25)
        self.y_selector.setFixedWidth(400)
        self.y_selector.setFixedHeight(25)
        self.y_selector.setCurrentIndex(-1)

        self.layout.addWidget(QLabel("Item uid:"))
        self.layout.addWidget(self.y_selector)
        self.imgshow_button = QPushButton("Show Image")

        self.layout.addWidget(self.imgshow_button)
        self.imageLayout.addWidget(self.result)
        active_search_model = self.model.search
        active_search_model.events.active_run.connect(self._on_active_run_selected)
        self.imgshow_button.clicked.connect(self._on_imgshow_button_clicked)

    def _on_imgshow_button_clicked(self, event):
        """
        Click on the button, and the RasteredImage result is shown.
        """
        import databroker
        catalog = databroker.catalog['test']
        active_run = catalog[self.y_selector.currentText()]
        try:
            shape = active_run.metadata["start"]["shape"]
            field_list = active_run.metadata["start"]["detectors"]
            field = field_list[0]
            image = RasteredImages(field, shape=shape)
            view = QtFigure(image.figure)
            image.add_run(active_run)
            self.result.addTab(view, field)
        except KeyError as e:
            print("KeyError: ", e)

    def _on_active_run_selected(self, event):
        """
        Get the item uid.
        """
        self.y_selector.clear()
        run = self.model.search.active_run
        uid = run.metadata["start"]["uid"]
        self.y_selector.addItem(uid)

    def onTabCloseRequested(self, index):
        """
        Remove a tab with the specified index, but first deletes the widget it contains. 
        """
        widget = self.result.widget(index)
        if widget:
            widget.deleteLater()
        self.result.removeTab(index)
