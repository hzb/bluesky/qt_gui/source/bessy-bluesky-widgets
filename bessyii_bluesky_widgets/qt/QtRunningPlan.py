import time

from qtpy.QtWidgets import (
    QWidget,
    QGroupBox,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QScrollArea,
    QLabel,
    QComboBox,
    QStatusBar,
    QFrame,
    QProgressBar,
)
import json

from qtpy.QtCore import Qt, Signal, Slot, QTimer
from bluesky_widgets.qt.threading import FunctionWorker

from datetime import datetime

class QtReRunningPlan(QWidget):
    signal_scan_status_changed = Signal(object, object)
    signal_running_item_changed = Signal(object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.documents = model.documents
        self.re_manager = model.run_engine         

        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("Running Plan: "))
        
        self.name = QLabel()
        self.name.setStyleSheet("color: darkgreen")
        self.start = QLabel()
        self.start.setStyleSheet("color: darkgreen")
        self.id = QLabel()
        self.id.setStyleSheet("color: darkgreen")
        self.id.mousePressEvent = self.lb_id_clicked
        hbox.addWidget(self.name)
        hbox.addStretch()
        hbox.addWidget(self.start)
        hbox.addStretch()
        hbox.addWidget(self.id)


        # Create the layout for the new widget
        vbox = QVBoxLayout()
        vbox.addLayout(hbox)
        
        self.arguments = QLabel()
        self.arguments.setStyleSheet("color: darkgreen")
        self.arguments.setWordWrap(True)

        self.progress = QProgressBar()
        self.progress.setRange(0, 100)
        self.progress.setVisible(False)
        vbox.addWidget(self.arguments)
        vbox.addWidget(self.progress)

        box = QVBoxLayout()
        frame = QFrame()
        frame.setFrameShape(QFrame.StyledPanel)
        frame.setLineWidth(3)
        frame.setLayout(vbox)
        box.addWidget(frame)

        
        self.setLayout(box)

        self._is_item_running = False
        self._plan_progress = 0
        self._plan_length = 0

        self.documents.events.scan_status.connect(self.on_scan_status_changed)
        self.signal_scan_status_changed.connect(self.slot_scan_status_changed)

        self.documents.events.new_data.connect(self.on_new_data)

        self.re_manager.events.running_item_changed.connect(self.on_running_item_changed)
        self.signal_running_item_changed.connect(self.slot_running_item_changed)

    def lb_id_clicked(self, *args, **kwargs):
        txt = self.id.text().split(':')[1].strip()
        print(txt)

    def on_running_item_changed(self, event):
        if not self._is_item_running:
            self.signal_running_item_changed.emit(event.running_item)

    @Slot(object)
    def slot_running_item_changed(self, running_item):
        if running_item:
            self.arguments.setText('Scan in Progress.')
        else:
            self.arguments.setText('')

    def on_new_data(self, event):
        if self._is_item_running:
            if not 'primary' in self.documents.event_streams.keys():
                return
            if event.data['data'].keys() == self.documents.event_streams['primary'].keys():
                self._plan_progress = self._plan_progress + 1
                _progr = int(100 * (self._plan_progress / self._plan_length))
                self.progress.setValue(_progr)

    def on_scan_status_changed(self, event):
        status = event.status
        data = event.data
        self.signal_scan_status_changed.emit(status, data)

    @Slot(object, object)
    def slot_scan_status_changed(self, status, data):

        if status == 'start':
            self._is_item_running = True
            self.name.setText('Name: ' + data['plan_name'])
            self.id.setText('Scan ID: ' + str(data['scan_id']))
            time = data['time']
            dt_object = datetime.fromtimestamp(time)
            self.start.setText('Start: ' + dt_object.strftime("%m/%d/%Y, %H:%M:%S"))

            arguments = ''

            if 'plan_args' in data.keys():
                for arg in data['plan_args'].keys():
                    if not arg == 'detectors':
                        arguments = arguments + arg + ': ' + str(data['plan_args'][arg]) + '  '
            
            self.arguments.setText(arguments)

            if 'num_points' in data.keys():
                self._plan_length = data['num_points']
            else:
                self._plan_length = 100
            self.progress.setVisible(True)
                
        elif status == 'stop':
            self.name.setText('')
            self.start.setText('')
            self.id.setText('')
            self.arguments.setText('')
            self.progress.setVisible(False)
            self.progress.setValue(0)
            self._plan_length = 0
            self._plan_progress = 0
            self._is_item_running = False