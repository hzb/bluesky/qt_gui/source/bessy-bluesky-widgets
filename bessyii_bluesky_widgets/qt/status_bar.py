import time

from qtpy.QtWidgets import (
    QWidget,
    QGroupBox,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QComboBox,
    QStatusBar,
    QMenu,
    QAction,
)
from qtpy.QtCore import Qt, Signal, Slot, QTimer
from qtpy.QtGui import QCursor
from bluesky_widgets.qt.threading import FunctionWorker

class QtStatusBar(QStatusBar):
    def __init__(self, parent, model, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.model = model
        hbox = QHBoxLayout()
        self._re_connect = QtReManagerConnection(self.model.run_engine)
        hbox.addWidget(self._re_connect)
        hbox.addStretch(1)
        self._env_state = QtReEnvironmentControls(self.model.run_engine)
        hbox.addWidget(self._env_state)
        hbox.addStretch(1)
        self._re_state = QtReStatusMonitor(self.model.run_engine)
        hbox.addWidget(self._re_state)
        #to be able to use layouts we need to pack this into a widget
        self._bar_widget = QWidget(parent)
        self._bar_widget.setLayout(hbox)
        self.addPermanentWidget(self._bar_widget, 1)
    
    def contextMenuEvent(self, event):
        self.menu = QMenu(self)
        default = QAction('Default', self)
        yellow = QAction('Yellow', self)
        green = QAction('Green', self)
        blue = QAction ('Blue', self)
        default.triggered.connect(lambda: self.change_color('default'))
        yellow.triggered.connect(lambda: self.change_color('yellow'))
        green.triggered.connect(lambda: self.change_color('green'))
        blue.triggered.connect(lambda: self.change_color('blue'))
        self.menu.addAction(default)
        self.menu.addAction(yellow)
        self.menu.addAction(green)
        self.menu.addAction(blue)
        self.menu.popup(QCursor.pos())
    
    def change_color(self, color):
        if color == 'default':
            self.setStyleSheet("")
        else:
            self.setStyleSheet("background-color : " + color)
        

class QtReManagerConnection(QWidget):

    signal_update_widget = Signal(object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model

        self._re_control = QLabel('Connection: ')
        self._lb_connected = QLabel("OFF")

        hbox = QHBoxLayout()
        hbox.addWidget(self._re_control)
        hbox.addWidget(self._lb_connected)

        self.setLayout(hbox)

        # Thread used to initiate periodic status updates
        self._thread = None
        self.updates_activated = False
        self._deactivate_updates = False
        self.update_period = 0.5  # Status update period in seconds

        self._update_widget_states()
        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widget.connect(self.slot_update_widgets)

        self._first_connection = False
        self._pb_re_manager_connect_clicked()

    def _update_widget_states(self):
        # We don't know if the server is online or offline:
        self._lb_connected.setText("-----")
        self._lb_connected.setStyleSheet("color: yellow")

    def on_update_widgets(self, event):
        try:
            is_connected = event.is_connected
            self.signal_update_widget.emit(is_connected)
        except Exception as e:
            print(e)

    @Slot(object)
    def slot_update_widgets(self, is_connected):
        # 'is_connected' may take values None, True and False
        text = "-----"
        color = "yellow"
        if is_connected is True:
            text = "ONLINE"
            color = "green"
        elif is_connected is False:
            text = "OFFLINE"
            color = "red"
        self._lb_connected.setText(text)
        self._lb_connected.setStyleSheet("color: " + color)

    def _pb_re_manager_connect_clicked(self):
        self.updates_activated = True
        self._deactivate_updates = False
        self.model.clear_connection_status()
        self._update_widget_states()

        # TODO: If the history contains large number of plans and the program is
        #   disconnected and connected again (Disconnect then Connect buttons), the program
        #   is likely to freeze (on the stage of inserting items into the table).
        #   `self._first_connection` is used to prevent data from reloading if this is not
        #   the first connection. The data is still reloaded once status is checked.
        #   The reason why the program is freezing is not apparent and it would be useful
        #   to find the exact reason why this is happening.
        if not self._first_connection:
            self.model.manager_connecting_ops()
            self._first_connection = True

        self._start_thread()
        

    def _pb_re_manager_disconnect_clicked(self):
        self._deactivate_updates = True
        self._update_widget_states()

    def _start_thread(self):
        self._thread = FunctionWorker(self._reload_status)
        self._thread.finished.connect(self._reload_complete)
        self._thread.start()

    def _reload_complete(self):
        if not self._deactivate_updates:
            self._start_thread()
        else:
            self.model.clear_connection_status()
            self.updates_activated = False
            self._deactivate_updates = False
            self._update_widget_states()

    def _reload_status(self):
        self.model.load_re_manager_status()
        time.sleep(self.update_period)

class QtReEnvironmentControls(QWidget):
    signal_update_widget = Signal(bool, object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model

        self._env_label = QLabel('Environment: ')
        self._env_state = None
        self._env_btn = QComboBox()
        self._env_btn.textActivated.connect(self._env_action_selected)
        self._env_btn.setEnabled(False)
        hbox = QHBoxLayout()
        hbox.addWidget(self._env_label)
        hbox.addWidget(self._env_btn)

        '''

        self._pb_env_open = QPushButton("Open")
        self._pb_env_open.setEnabled(False)
        self._pb_env_open.clicked.connect(self._pb_env_open_clicked)

        self._pb_env_close = QPushButton("Close")
        self._pb_env_close.setEnabled(False)
        self._pb_env_close.clicked.connect(self._pb_env_close_clicked)

        self._pb_env_destroy = QPushButton("Destroy")
        self._pb_env_destroy.setEnabled(False)
        self._pb_env_destroy.clicked.connect(self._pb_env_destroy_clicked)

        '''
        self.setLayout(hbox)

        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widget.connect(self.slot_update_widgets)

    def _env_action_selected(self, action):
        if action == '' or action == self._env_state:
            return
        if action == 'Open':
            self._pb_env_open_clicked()
        elif action == 'Close':
            self._pb_env_close_clicked()
        elif action == 'Destroy':
            self._pb_env_destroy_clicked()
        else:
            raise ValueError('Selected State is not valid!')


    def on_update_widgets(self, event):
        is_connected = bool(event.is_connected)
        status = event.status
        self.signal_update_widget.emit(is_connected, status)

    @Slot(bool, object)
    def slot_update_widgets(self, is_connected, status):
        # 'is_connected' takes values True, False
        worker_exists = status.get("worker_environment_exists", False)
        manager_state = status.get("manager_state", None)
        env_destroy_activated = self.model.env_destroy_activated
        self._env_state = "Open" if worker_exists else "Close"
        if not is_connected:
            return
        if not manager_state == 'idle':
            self._env_btn.setEnabled(False)
        else:
            self._env_btn.setEnabled(True)

        possible_actions = ['Open', 'Close']
        if env_destroy_activated:
            possible_actions.append('Destroy')

        self._env_btn.clear()
        self._env_btn.addItems(possible_actions)

        state = 'Open' if worker_exists else 'Close'

        self._env_btn.setCurrentText(state) 
        '''
        self._pb_env_open.setEnabled(is_connected and not worker_exists and (manager_state == "idle"))
        self._pb_env_close.setEnabled(is_connected and worker_exists and (manager_state == "idle"))
        self._pb_env_destroy.setEnabled(is_connected and worker_exists and env_destroy_activated)
        '''

    def _pb_env_open_clicked(self):
        try:
            self.model.environment_open()
        except Exception as ex:
            print(f"Exception: {ex}")

    def _pb_env_close_clicked(self):
        try:
            self.model.environment_close()
        except Exception as ex:
            print(f"Exception: {ex}")

    def _pb_env_destroy_clicked(self):
        try:
            self.model.environment_destroy()
        except Exception as ex:
            print(f"Exception: {ex}")

        
class QtReStatusMonitor(QWidget):
    signal_update_widget = Signal(object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self._states = {}
        self._states["RE Environment"] = "-"
        self._states["Manager state"] = "-"
        self._states["RE state"] = "-"
        self._states["Items in history"] = "-"
        self._states["Queue is running"] = "-"
        self._states["Queue STOP pending"] = "-"
        self._states["Items in queue"] = "-"
        self._states["Queue LOOP mode"] = "-"


        hbox = QHBoxLayout()
        self._re_status = QLabel(self._states["RE state"])
        self._re_descr = QLabel('Status: ')
        hbox.addWidget(self._re_descr)
        hbox.addWidget(self._re_status)
        self.setLayout(hbox)

        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widget.connect(self.slot_update_widgets)

        self.setToolTip('-')

    def _get_tooltip(self):
        return '\n'.join([str(a) + ': ' + str(b) for a, b in self._states.items()])

    def on_update_widgets(self, event):
        status = event.status
        self.signal_update_widget.emit(status)

    @Slot(object)
    def slot_update_widgets(self, status):
        worker_exists = status.get("worker_environment_exists", None)
        manager_state = status.get("manager_state", None)
        re_state = status.get("re_state", None)
        items_in_history = status.get("items_in_history", None)
        items_in_queue = status.get("items_in_queue", None)
        queue_is_running = bool(status.get("running_item_uid", False))
        queue_stop_pending = status.get("queue_stop_pending", None)

        queue_mode = status.get("plan_queue_mode", None)
        queue_loop_enabled = queue_mode.get("loop", None) if queue_mode else None

        # Capitalize state of RE Manager
        manager_state = manager_state.upper() if isinstance(manager_state, str) else manager_state
        re_state = re_state.upper() if isinstance(re_state, str) else str(re_state).upper()

        self._states["RE Environment"] = "OPEN" if worker_exists else "CLOSED"
        self._states["Manager state"] = manager_state
        self._states["RE state"] = re_state
        self._re_status.setText(re_state)
        self._states["Items in history"] = str(items_in_history)
        self._states["Queue is running"] = str(items_in_queue)
        self._states["Queue STOP pending"] = "YES" if queue_is_running else "NO"
        self._states["Items in queue"] = "YES" if queue_stop_pending else "NO"
        self._states["Queue LOOP mode"] = "ON" if queue_loop_enabled else "OFF"

        self.setToolTip(self._get_tooltip())