from PyQt5 import QtGui, QtCore,QtWidgets
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from qtpy.QtCore import Qt, Signal, Slot, QTimer, QPoint

from qtpy.QtWidgets import (
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
    QGridLayout,
    QFrame,
    QSizePolicy,
    QRadioButton,
    QPushButton,
    QGroupBox,
    QLabel,
    QFileDialog,
    QLineEdit,
    QDialog,
    QListWidget,
    QMenu,
    QScrollArea,
    QTableWidget,
    QTableWidgetItem,
    QAbstractScrollArea,
)

class QtBaselineSearch(QWidget):
    signal_update_widget = Signal(object)
    def __init__(self, *args, model, **kwargs):
        super().__init__(*args, **kwargs)
        self.search = model.search
        self.run_engine = model.run_engine
        #frame = QFrame()
        self.search.events.active_run.connect(self.select_run)
        
        hbox = QHBoxLayout()
        vbox1 = QVBoxLayout()
        title = QLabel('Selected Baseline')
        vbox1.addWidget(title)
        self.selected_baseline = QtBaselineViewer(datasets = None)
        vbox1.addWidget(self.selected_baseline)
        hbox.addLayout(vbox1)

        vbox2 = QVBoxLayout()
        title2 = QLabel('Current Baseline')
        vbox2.addWidget(title2)
        self.current_baseline = QtBaselineViewer(datasets = None)
        vbox2.addWidget(self.current_baseline)
        hbox.addLayout(vbox2)
        
        vbox_top = QVBoxLayout()
        vbox_top.addLayout(hbox)
        self.change_btn = QPushButton('Change Baseline')
        self.change_btn.clicked.connect(self.change_baseline)
        self.change_btn.setEnabled(False)
        vbox_top.addWidget(self.change_btn)

        self.lie_about_current_baseline()

        self.setLayout(vbox_top)

        self.run_engine.events.status_changed.connect(self.on_update)
        self.signal_update_widget.connect(self.slot_update_widgets)
    
    def on_update(self, event):
        self.signal_update_widget.emit(event)

    @Slot(object)
    def slot_update_widgets(self, event):
        if not event is None:
            if event.is_connected:
                self.change_btn.setEnabled(True)
            else:
                self.change_btn.setEnabled(False)

    def select_run(self, event):
        uid = event.uid
        run = event.run
        if hasattr(run, 'baseline'):
            self.selected_baseline.datasets = {str(uid): run}
        else:
            self.selected_baseline.datasets = None

        self.selected_baseline.populate()
    
    def change_baseline(self):
        item = {'item_type': 'plan',
                'name': 'change_baseline',
                'args': [],
                'kwargs': {},
                'meta': {}}
        self.run_engine.queue_item_add(item=item)
        
    
    def lie_about_current_baseline(self):
        datasets = dict()
        for uid, run in self.search.current_catalog.items():
            if hasattr(run, 'baseline'):
                datasets[str(uid)] = run
                self.current_baseline.datasets = datasets
                self.current_baseline.populate()
                return

class QtBaselineDialog(QDialog):
    def __init__(self, *args, datasets, **kwargs):
        super().__init__(*args, **kwargs)
        self.datasets = datasets
        self.setWindowTitle('Dataviewer')
        self.setWindowModality(Qt.ApplicationModal)

        box = QVBoxLayout()
        self.viewer = QtBaselineViewer(datasets = datasets)
        box.addWidget(self.viewer)

        self.setLayout(box)
        self.exec_()


class QtBaselineViewer(QWidget):
    def __init__(self, *args, datasets, **kwargs):
        super().__init__(*args, **kwargs)
        self.datasets = datasets

        box = QVBoxLayout()

        self.label = QLabel('No Baseline found!')

        box.addWidget(self.label)

        self.table = QTableWidget()
        self.table.setMinimumHeight(600)
        self.table.setMinimumWidth(400)
        self.table.horizontalHeader().setSectionsMovable(True)
        self.table.horizontalHeader().setStyleSheet("font:11pt")
        self.table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        box.addWidget(self.table)
        
        self.populate()

        self.setLayout(box)

    def populate(self):
        self.table.setColumnCount(0)
        self.table.setRowCount(0)
        if self.datasets is None:
            self.label.show()
        else:
            self.label.hide()
            list_of_dets = []
            self.table.setColumnCount(len(self.datasets.keys()) + 1)
            baseline = {}
            for uid in self.datasets.keys():
                scanid = self.datasets[uid].metadata['start']['scan_id']
                baseline[scanid] = self.datasets[uid].baseline.read()
                baseline[scanid]['UID'] = uid
                for key in baseline[scanid].keys():
                    if not key in list_of_dets:
                        list_of_dets.append(key)
            self.table.setRowCount(len(list_of_dets))
            
            for i in range(len(list_of_dets)):
                self.table.setItem(i, 0, QTableWidgetItem(list_of_dets[i]))
                col = 1
                for sid in baseline.keys():
                    if list_of_dets[i] in baseline[sid].keys():
                        try:
                            val = baseline[sid][list_of_dets[i]].values[0]
                        except IndexError as e:
                            val = baseline[sid][list_of_dets[i]].values
                        self.table.setItem(i, col, QTableWidgetItem(str(val)))
                    col += 1
            self.table.setHorizontalHeaderLabels([''] + [str(x) for x in baseline.keys()])
            self.table.resizeColumnsToContents()