import ast
import re
import yaml
import numpy as np

from bluesky_widgets.qt.run_engine_client import _QtRePlanEditorTable, _QtReViewer, _QtReEditor

try:
    # cytools is a drop-in replacement for toolz, implemented in Cython
    from cytools import partition
except ImportError:
    from toolz import partition

from qtpy.QtWidgets import (
    QWidget,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QTableWidget,
    QTableWidgetItem,
    QHeaderView,
    QTabWidget,
    QComboBox,
    QFormLayout,
 
    QCheckBox,
    QLineEdit,
    QCompleter,
    QStyledItemDelegate,
)
from qtpy.QtCore import Signal, Slot

from PyQt5.QtCore import Qt


class ReadOnlyDelegate(QStyledItemDelegate):
    """
    The content applied with this class can not be changed.
    """
    def createEditor(self, parent, option, index):
        return

from PyQt5 import QtCore, QtGui, QtWidgets


class CollapsibleBox(QtWidgets.QWidget):
    def __init__(self, title="", parent=None):
        super(CollapsibleBox, self).__init__(parent)

        self.toggle_button = QtWidgets.QToolButton(
            text=title, checkable=True, checked=False
        )
        self.toggle_button.setStyleSheet("QToolButton { border: none; }")
        self.toggle_button.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextBesideIcon
        )
        self.toggle_button.setArrowType(QtCore.Qt.RightArrow)
        self.toggle_button.pressed.connect(self.on_pressed)

        self.toggle_animation = QtCore.QParallelAnimationGroup(self)

        self.content_area = QtWidgets.QScrollArea(
            maximumHeight=0, minimumHeight=0
        )
        self.content_area.setSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed
        )
        self.content_area.setFrameShape(QtWidgets.QFrame.NoFrame)

        lay = QtWidgets.QVBoxLayout(self)
        lay.setSpacing(0)
        lay.setContentsMargins(0, 0, 0, 0)
        lay.addWidget(self.toggle_button)
        lay.addWidget(self.content_area)

        self.toggle_animation.addAnimation(
            QtCore.QPropertyAnimation(self, b"minimumHeight")
        )
        self.toggle_animation.addAnimation(
            QtCore.QPropertyAnimation(self, b"maximumHeight")
        )
        self.toggle_animation.addAnimation(
            QtCore.QPropertyAnimation(self.content_area, b"maximumHeight")
        )

    @QtCore.pyqtSlot()
    def on_pressed(self):
        checked = self.toggle_button.isChecked()
        self.toggle_button.setArrowType(
            QtCore.Qt.DownArrow if not checked else QtCore.Qt.RightArrow
        )
        self.toggle_animation.setDirection(
            QtCore.QAbstractAnimation.Forward
            if not checked
            else QtCore.QAbstractAnimation.Backward
        )
        self.toggle_animation.start()

    def setContentLayout(self, layout):
        lay = self.content_area.layout()
        del lay
        self.content_area.setLayout(layout)
        collapsed_height = (
            self.sizeHint().height() - self.content_area.maximumHeight()
        )
        content_height = layout.sizeHint().height()
        for i in range(self.toggle_animation.animationCount()):
            animation = self.toggle_animation.animationAt(i)
            animation.setDuration(50)
            animation.setStartValue(collapsed_height)
            animation.setEndValue(collapsed_height + content_height)

        content_animation = self.toggle_animation.animationAt(
            self.toggle_animation.animationCount() - 1
        )
        content_animation.setDuration(50)
        content_animation.setStartValue(0)
        content_animation.setEndValue(content_height)


class AddCustomPlanWidget(QTableWidget):
    signal_update_widgets = Signal(bool)
    signal_switch_tab = Signal(str)
    signal_allowed_plan_changed = Signal()

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widgets.connect(self.slot_update_widgets)
        self.detectors_list =  [None, 'kth19', 'kth20','kth25','kth26','oaese.kth00', 'oaese.kth01', 's1_chan', 'ea_counter']
        self.kth_ranges = ["AUTO", "20 nA", "20 mA"]
        self.default_parameters_dict ={
            "scan": {"detectors": " ", "mot": " ", "start": " ", "stop": " ", "num": 1, "per_step": "None", "md": "None"},
            "count": {"detectors": " ", "num": 1, "delay": "None", "per_shot": "None", "md": "None"},
            "flyscan": {"detectors": " ", "flyer": " ", "start": " ", "stop": " ", "vel": 0.2, "delay": 0.2, "md": "None"},
            "grid_scan": {"detectors": " ", "motor1": " ", "start1": " ", "stop1": " ", "motor2": "","start2": " ", "stop2": " ", "md": "None"},
            "xas": {"End_Station": ["OAESE", "SISSY1"],
                    "Mono": ["u17_dcm.en", "u17_pgm.en", "ue48_pgm.en"],
                    "Mode": ["step", "continuous"],
                    "I0" : " ",
                    "TFY": " ",
                    "TEY": " ",
                    "Velocity(eV/s)": ["0.5", "1"], 
                    "Start_Energy": " ",
                    "Stop_Energy": " ",
                    "Step_Size(eV)": " ",
                    "Use_shutter": ["False", "True"],
                    "md": "None"},
            "mv": {"mot": " ", "set_point": " "},
            "mvr": {"mot": " ", "set_point": " "}
            }
        
        # If value is none then allow anything, if it's a list then use a drop down menu
        # If the key contains a star then it is optional depending on previous non star key
        # If the key contains a ~ the use the list for the tooltip

        self.source_default_plan_parameters_dict = {"source": {'value': ['u17_pgm', 'u17_dcm',"ue48_pgm" ],'widget':None, "label_widget" : None, "label_name" : None},
                              "source*pgm*grating": {"value" : ["unchanged", "400 l/mm", "800 l/mm"],"widget" : None, "label_widget" : None, "label_name" : None},
                              "source*pgm*slit_width": {"value" : "unchanged","widget" : None, "label_widget" : None, "label_name" : None},
                              "source*dcm*crystal":{"value" : ["unchanged", "111", "222"],"widget" : None, "label_widget" : None, "label_name" : None},
                              "gap": {"value" : "unchanged","widget" : None, "label_widget" : None, "label_name" : None}, 
                              "source*ue48*shift": {"value" : "unchanged","widget" : None, "label_widget" : None, "label_name" : None},
                            }
        
        self.sample_default_plan_parameters_dict = {"name":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None}, 
                              "description":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None}}
        
        self.metadata_default_plan_parameters_dict = {"reason":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None}}
        
        self.default_plan_parameters_dict = {
            "xas": {"xas":{"mono":{"value" : ["ue48_pgm.en","u17_dcm.en", "u17_pgm.en", "motor", "fdev"],"widget" : None, "label_widget" : None, "label_name" : None},
                           "harmonic":{"value" : ["max flux","0", "1","2","3", "4", "5"],"widget" : None, "label_widget" : None, "label_name" : None},
                           "order":{"value" : ["0", "1","2","3", "4", "5"],"widget" : None, "label_widget" : None, "label_name" : None},
                           "slit_width":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None, "enabled_when":{"mono":"pgm"}},
                           "start_energy": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "stop_energy": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "mode":  {"value" : ["continuous", "step"],"widget" : None, "label_widget" : None, "label_name" : None,"forced_by":{"step":{"mono":"dcm"}}}, # output is forced to be continuous when the value of mono contains "dcm"
                           "velocity":{"value" : ["0.1","0.5","1"],"widget" : None, "label_widget" : None, "label_name" : None, "enabled_when":{"mode":"continuous"}}, 
                           "step_size":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None,"enabled_when":{"mode":"step"}},
                           "int_time" : {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "I0": {"value" : self.detectors_list,"widget" : None,"tooltip":"Test"},
                           "I0_kth_range":{"value" : self.kth_ranges,"widget" : None,"label_widget" : None, "label_name" : "kth range","enabled_when":{"I0":"kth"}},
                           "TEY": {"value" : self.detectors_list,"widget" : None,"label_widget" : None, "label_name" : None},
                           "TEY_kth_range":{"value" : self.kth_ranges,"widget" : None,"label_widget" : None, "label_name" : "kth range","enabled_when":{"TEY":"kth"}},
                           "TFY0": {"value" : self.detectors_list,"widget" : None,"label_widget" : None, "label_name" : None},
                           "TFY0_kth_range":{"value" : self.kth_ranges,"widget" : None,"label_widget" : None, "label_name" : "kth range","enabled_when":{"TFY0":"kth"}},
                           "TFY1": {"value" : self.detectors_list,"widget" : None,"label_widget" : None, "label_name" : None},
                           "TFY1_kth_range":{"value" : self.kth_ranges,"widget" : None,"label_widget" : None, "label_name" : "kth range","enabled_when":{"TFY1":"kth"}},
                           "TFY2": {"value" : self.detectors_list,"widget" : None,"label_widget" : None, "label_name" : None},
                           "TFY2_kth_range":{"value" : self.kth_ranges,"widget" : None,"label_widget" : None, "label_name" : "kth range","enabled_when":{"TFY2":"kth"}},
                           "shutter": {"value" : ["True", "False"],"widget" : None,"label_widget" : None, "label_name" : "Use Shutter"}
                           },
                    #"source": self.source_default_plan_parameters_dict,
                    "sample":self.sample_default_plan_parameters_dict,
                    "metadata":self.metadata_default_plan_parameters_dict
            },
            "scan": {"scan":{"motor":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "start": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "stop": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "num":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "detectors~": {"value" : self.detectors_list,"widget" : None},
                           },
                    #"source": self.source_default_plan_parameters_dict,
                    "sample":self.sample_default_plan_parameters_dict,
                    "metadata":self.metadata_default_plan_parameters_dict
            },
            "count": {"count":{"detectors~": {"value" : self.detectors_list,"widget" : None},
                      "num":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                      "delay":{"value" : 0.2,"widget" : None, "label_widget" : None, "label_name" : None}
                        },
                    #"source": self.source_default_plan_parameters_dict,
                    "sample":self.sample_default_plan_parameters_dict,
                    "metadata":self.metadata_default_plan_parameters_dict
            },
            "tune_centroid": {"tune_centroid":{"motor":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "start": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "stop": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "min_step":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "detector": {"value" : self.detectors_list,"widget" : None},
                           },
                    #"source": self.source_default_plan_parameters_dict,
                    "sample":self.sample_default_plan_parameters_dict,
                    "metadata":self.metadata_default_plan_parameters_dict
            },
            "move":{"move":{"motor":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                            "position":{"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                            "relative":{"value" : ["False", "True"],"widget" : None, "label_widget" : None, "label_name" : None}}
                    },
            "grid_scan": {"grid_scan":{
                           "x_motor": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "x_start": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "x_stop": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "x_num": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "y_motor": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "y_start": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "y_stop": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "y_num": {"value" : None,"widget" : None, "label_widget" : None, "label_name" : None},
                           "snaking":  {"value" : ["False", "True"],"widget" : None, "label_widget" : None, "label_name" : None},
                           "I0~": {"value" : self.detectors_list,"widget" : None},
                           "detectors~": {"value" : self.detectors_list,"widget" : None}
                           },
                    #"source": self.source_default_plan_parameters_dict,
                    "sample":self.sample_default_plan_parameters_dict,
                    "metadata":self.metadata_default_plan_parameters_dict
            }
        }

        #### TODO -  Add flyscan, scan, tune_centroid

        #Itterate through the dict and make a new version using the defaults

        import copy

        self.plan_parameters_dict = copy.deepcopy(self.default_plan_parameters_dict)
        def traverse_dict(d):
            for k, v in d.items():
                if isinstance(v, dict):
                    traverse_dict(v)
                else:
                    if k == "value":

                        if isinstance(d['value'], list):

                            d['value'] = d['value'][0]

                      

        #do it recursiveley
        traverse_dict(self.plan_parameters_dict)
        #print(self.plan_parameters_dict)

        self.totalLayout = QVBoxLayout(self)
        self.chooseLayout = QHBoxLayout()

        self.combo = QComboBox(self)
        self.plan_dict = self.default_plan_parameters_dict
        self.combo_list = [" "] + list(self.plan_dict.keys())
        self.combo.addItems(self.combo_list)
        self.combo.currentTextChanged.connect(self.on_clicked)
        self.chooseLayout.addWidget(self.combo)
        self.combo.setEnabled(False)

        self.lineEdit = QLineEdit()
        self.currently_selected_plan = None
        self.chooseLayout.addWidget(self.lineEdit)

        self.v_box = QVBoxLayout()
        self._tableWidget = QTableWidget()

        # Make a collapsable menu
      
        self.scroll = QtWidgets.QScrollArea()
        self.v_box.addWidget(self.scroll)
        
        self.content = QtWidgets.QWidget()
        self.scroll.setWidget(self.content)
        self.scroll.setWidgetResizable(True)
        self.vlay = QtWidgets.QVBoxLayout(self.content)
        


        
        #self.v_box.addWidget(self._tableWidget2)
        self.totalLayout.addLayout(self.chooseLayout)
        self.totalLayout.addLayout(self.v_box)

        self._pb_add_to_queue = QPushButton("Add to Queue")
        self._pb_cancel = QPushButton("Cancel")
        self._pb_add_to_queue.setEnabled(False)
        self._pb_cancel.setEnabled(False)
        self._pb_add_to_queue.clicked.connect(self._pb_add_to_queue_clicked)
        self._pb_cancel.clicked.connect(self._pb_cancel_clicked)

        self.addMotor_button = QPushButton(self._tableWidget)
        self.addMotor_button.setText("Add motors")
        self.addMotor_button.clicked.connect(self.klick_addMotors)
        self.addMotor_button.setEnabled(False)

        self.hbox = QHBoxLayout()
        self.hbox.addStretch(1)
        self.hbox.addWidget(self._pb_add_to_queue)
        self.hbox.addWidget(self._pb_cancel)
        self.hbox.addWidget(self.addMotor_button)
        self.totalLayout.addLayout(self.hbox)

        self.insert_motornumber = None
        self.param_list = None
        self.param_dict = None

    def on_update_widgets(self, event):
        # None should be converted to False:
        is_connected = bool(event.is_connected)
        self.signal_update_widgets.emit(is_connected)

        


  

    def create_expandable(self):

        """
        Called when we initially select a new plan type
        
        """

        #print(f"Calling create_expandable with {self.currently_selected_plan}")

       

        layout = self.v_box
        for i in reversed(range(layout.count())): 
            layout.itemAt(i).widget().deleteLater()

        self.scroll = QtWidgets.QScrollArea()
        self.v_box.addWidget(self.scroll)
        
        self.content = QtWidgets.QWidget()
        self.scroll.setWidget(self.content)
        self.scroll.setWidgetResizable(True)
        self.vlay = QtWidgets.QVBoxLayout(self.content)

        
     

        for key, item in self.default_plan_parameters_dict[str(self.currently_selected_plan)].items():

            # Create an expandable header with that key name
            box = CollapsibleBox(key)
            self.vlay.addWidget(box)
                # creating a form layout
            form_lay = QFormLayout()
            # For each of the options in the group expand those options
            for option_key, option_item in item.items():

                option_key_string = option_key

                default_param = self.default_plan_parameters_dict[self.currently_selected_plan][key][option_key]
                param = self.plan_parameters_dict[self.currently_selected_plan][key][option_key]

                if "value" in default_param:

                    widget = None
                    options = default_param["value"]
                    
                    if isinstance(options,list):

                        """
                        If it's a list then use a combo box
                        """

                        widget = QComboBox()
                        param['widget'] = widget

                        widget.addItems(options)
                        widget.currentTextChanged.connect(lambda: self._update_selections())
                    
                    else:

                        param['widget'] = QLineEdit()
                        widget = param['widget']
                        
                        
                        widget.setText(str(param['value']))

                        #if "~" in option_key:
                         #   options_string = ", ".join(str(x) for x in options)
                          #  widget.setToolTip(f"Choose from: {options_string}")

                        widget.textChanged.connect(lambda: self._update_selections())   

                    if widget:

                        
                        if 'label_name' in default_param:
                            if default_param['label_name']:
                                label_widget = QtWidgets.QLabel(default_param['label_name'])
                            else:
                                label_widget = QtWidgets.QLabel(option_key_string)
                        else:
                            label_widget = QtWidgets.QLabel(option_key_string)


                        label_widget.setFixedWidth(100)
                        param['label_widget'] = label_widget


                        form_lay.addRow(label_widget,widget)

               


            box.setContentLayout(form_lay)
        self.vlay.addStretch()
        self.scroll.show()
        
     

    def update_expandable(self):

        """
        Called every time we either select a new plan, or update the selections for that plan
        
        """

        for key, item in self.default_plan_parameters_dict[str(self.currently_selected_plan)].items():

            for option_key, option_item in item.items():

                #Strip the key of any trailing ~ which is used to signify to us that we want to use this list as a tooltip
                option_key_string = option_key.replace("~","")
                default_param = self.default_plan_parameters_dict[self.currently_selected_plan][key][option_key]
                param = self.plan_parameters_dict[self.currently_selected_plan][key][option_key]

                if "value" in default_param:

                    if "enabled_when" in param:

                            enabled_when = param["enabled_when"]
                            row_widget = param['widget']
                            row_label = param['label_widget']

                            for parent_param, required_value in enabled_when.items():
                                

                                if  required_value in self.plan_parameters_dict[self.currently_selected_plan][key][parent_param]['value']:

                                    row_widget.setDisabled(False)
                                    row_label.setDisabled(False)

                                elif row_widget != None:
                                    row_widget.setDisabled(True)
                                    row_label.setDisabled(True)

        self.vlay.addStretch()
        self.scroll.show()
        
       

    def _update_selections(self):

        """
        When plan parameters are updated, update the dict
        """
        def traverse_dict(d,key):
            for k, v in d.items():
                if isinstance(v, dict):
                    traverse_dict(v,k)
                else:
                    if k == "widget" and v:

                        if isinstance(v, QComboBox):

                            if "forced_by" in d:

                                """
                                For each possible forced value itterate through the dependencies. 

                                If all dependencies are met then the output is forced to be this
                                """
                                
                                for forced_output, dependencies in d["forced_by"].items():

                                    force = False
                                    for dependency_key, dependency_value in dependencies.items():

                                        #print(f"currently selected plan: {self.currently_selected_plan}, key: {key}, The depencies are {dependencies}, dependency_key: {dependency_key}, dependency_value: {dependency_value}")

                                        #print(f"The value of the dependency {dependency_key} is {self.plan_parameters_dict[self.currently_selected_plan][self.currently_selected_plan][dependency_key]['value']}")

                                        if dependency_value in self.plan_parameters_dict[self.currently_selected_plan][self.currently_selected_plan][dependency_key]["value"] :
                                            #print(f"There is a dependency on {dependency_key} and it is met: {dependency_value}")
                                            
                                            force = True
                                        else:
                                            #Else if any of the dependency keys do not have the required values, then do not force
                                            force = False
                                    
                                    if force:

                                        d['value'] = forced_output
                                        d['widget'].setCurrentText(forced_output)

                                    else:

                                        d['value'] = v.currentText()

                            else:
                                d['value'] = v.currentText()


                          
                            

                        else:
                            d['value'] = v.text()
                      

        #do it recursiveley
        traverse_dict(self.plan_parameters_dict[self.currently_selected_plan],"")

        
        self.update_expandable()

    @Slot()
    def slot_update_widgets(self):
        self._update_widget_state()

    def _update_widget_state(self):

        is_connected = bool(self.model.re_manager_connected)

        #self._rb_item_plan.setEnabled(not self._edit_mode_enabled)
        #self._rb_item_instruction.setEnabled(not self._edit_mode_enabled)
        self.combo.setEnabled(is_connected)

        self._pb_add_to_queue.setEnabled(is_connected)
    
        
       

    def _clear_widgets(self):
        """
        Clear the widgets
        """
        for row in reversed(range(self._tableWidget.rowCount())):
            self._tableWidget.removeRow(row)

    def _pb_add_to_queue_clicked(self):

        ##TODO - Create the item from the dictionary. How should I deal with performing a config. (I think I will make special plans)
        """
        Add item to queue
        """
        if self.currently_selected_plan != " ":
           
            item = self.setParamItem()
            #print(f"item {item}")
            if item is None:
                self.lineEdit.setText("Input values are not valid")
            else:
                self.model.queue_item_add(item=item)
                self.signal_switch_tab.emit("view")
                self._edit_mode_enabled = False
                self._current_item_source = ""
                reEditor = _QtReEditor(self.model)
                reEditor._show_item_preview()
                self.lineEdit.setText(" ")
          

        else:
            self.lineEdit.setText("please choose a plan")

    def _pb_cancel_clicked(self):
        """
        Cancel the plan.
        """
        self.combo.setCurrentText(" ")
        self._pb_add_to_queue.setEnabled(False)
        self._pb_cancel.setEnabled(False)
        self._edit_mode_enabled = False
        self._queue_item_type = ""
        self._queue_item_name = ""
        self._current_item_source = ""
        reEditor = _QtReEditor(self.model)
        reEditor._show_item_preview()
        self._clear_widgets()
        self.lineEdit.setText(" ")
        reEditor = _QtReEditor(self.model)
        reEditor._show_item_preview()
        self.lineEdit.setText("please choose a plan")

    def on_combobox_func(self, text):
        """
        Get the chosen plan name as currently_selected_plan
        """
        self.currently_selected_plan = text

    def on_clicked(self, text):
        """
        Create table of the chosen plan with clicking the button. Only "scan", "mv", "mvr" have the add-motor function.
        """
        self.currently_selected_plan = text
     
        if text != " ":
            self.lineEdit.setText("Set {} parameters...".format(text))
            #self.createTable()
            self.create_expandable()
            self.update_expandable()
            self.addMotor_button.setEnabled(False)
            if self.currently_selected_plan in ["scan", "mv", "mvr"]:
                self.addMotor_button.setEnabled(True)
        else:
            self.lineEdit.setText("please choose a plan...")
            self._clear_widgets()
            self._pb_add_to_queue.setEnabled(False)
            self._pb_cancel.setEnabled(False)
            self.addMotor_button.setEnabled(False)
    

    def checkInput(self, dict_item):
        """
        Check the input. The default value" " area must be filled in. The default "None" area can still be "None".
        """
        if dict_item is not None:
            value_list = []
            for key in dict_item.keys():
                value = dict_item.get(key)
                if value == " ":
                    return None
                else:
                    value_list.append(value)
            if all(v in ["None", " ", "NONE"] for v in value_list):
                return None
            else:
                return dict_item
        else:
            return None

    def setParamItem(self):
        """
        Set the dict item of the plans. The item consists of the default parameters and the inputs.
        """
        command = self.currently_selected_plan
        item = {}
        plan_dict = self.plan_parameters_dict[command][command]

        md = {}
        sample_dict = {}

        if "metadata" in self.plan_parameters_dict[command]:
            for key, value in self.plan_parameters_dict[command]["metadata"].items():
                md[key] = value['value']
        
        if "sample" in self.plan_parameters_dict[command]:

            for key, value in self.plan_parameters_dict[command]["sample"].items():
                sample_dict[key] = value['value']

            md['sample'] = sample_dict

        if command == "xas":

            mono = plan_dict["mono"]['value']
            start = float(plan_dict["start_energy"]['value'])
            stop = float(plan_dict["stop_energy"]['value'])
            mode = plan_dict["mode"]['value']
            int_time = plan_dict["int_time"]['value']

            I0 = plan_dict["I0"]['value']
            det_list = [I0]
            kth_ranges = []
            I0_kth_range = plan_dict["I0_kth_range"]['value']
            if I0_kth_range:
                kth_ranges.append(I0_kth_range)
            else:
                kth_ranges.append("NA")

            TEY = plan_dict["TEY"]['value']
            if TEY:
                det_list.append(TEY)
                TEY_kth_range = plan_dict["TEY_kth_range"]['value']
                if TEY_kth_range:
                    kth_ranges.append(TEY_kth_range)
                else:
                    kth_ranges.append("NA")
            
            
            TFY0 = plan_dict["TFY0"]['value']
            if TFY0:
                det_list.append(TFY0)
                TFY0_kth_range = plan_dict["TFY0_kth_range"]['value']
                if TFY0_kth_range:
                    kth_ranges.append(TFY0_kth_range)
                else:
                    kth_ranges.append("NA")

            TFY1 = plan_dict["TFY1"]['value']
            if TFY1:
                det_list.append(TFY1)
                TFY1_kth_range = plan_dict["TFY1_kth_range"]['value']
                if TFY1_kth_range:
                    kth_ranges.append(TFY1_kth_range)
                else:
                    kth_ranges.append("NA")

            TFY2 = plan_dict["TFY2"]['value']
            if TFY2:
                det_list.append(TFY2)
                TFY2_kth_range = plan_dict["TFY2_kth_range"]['value']
                if TFY2_kth_range:
                    kth_ranges.append(TFY2_kth_range)
                else:
                    kth_ranges.append("NA")


            md["I0"] = I0
            md["TEY"] = TEY
            md["TFY"] = [TFY0, TFY1, TFY2]
            md["technique"] = command

            if mode == "continuous":

                vel = plan_dict["velocity"]['value']

                item = {'item_type': 'plan',
                        'name': "xas_flyscan",
                        'args': [det_list, mono],
                        'kwargs': {'start':start, 'stop':stop,'vel': vel,'ranges':kth_ranges,'int_time':int_time,'md':md}
                    }
                
            elif mode == "step":

                step_size = float(plan_dict["mode*step*step_size"]['value'])
                num =  np.ceil(np.abs(stop-start)/step_size)
                list_args = [det_list, mono, start, stop]       

                item = {'item_type': 'plan',
                        'name': "xas_scan",
                        'args': list_args,
                        'kwargs': {'num': num,'ranges':kth_ranges,'int_time':int_time,'md':md}
                    }
            

        elif command == "scan":

            det_devices = plan_dict["detectors~"]['value']
            det_list = re.findall(r'[^,;\s]+', det_devices)
            mot_name = plan_dict["motor"]['value']
            start = float(plan_dict["start"]['value'])
            stop =float(plan_dict["stop"]['value'])
            number = int(plan_dict["num"]['value'])
            list_args = [det_list, mot_name, start, stop]         

            item = {'item_type': 'plan',
                        'name': command,
                        'args': list_args,
                        'kwargs': {'num': number,'md':md}
                    }
            
        elif command == "tune_centroid":

            det_devices = plan_dict["detectors~"]['value']
            det_list = re.findall(r'[^,;\s]+', det_devices)
            mot_name = plan_dict["motor"]['value']
            start = float(plan_dict["start"]['value'])
            stop =float(plan_dict["stop"]['value'])
            min_step = float(plan_dict["min_step"]['value'])
            list_args = [det_list,det_list[0], mot_name, start, stop, min_step]         

            item = {'item_type': 'plan',
                        'name': command,
                        'args': list_args,
                        'kwargs': {'md':md}
                    }
            
        elif command == "count":

            det_devices = plan_dict["detectors~"]['value']
            det_list = re.findall(r'[^,;\s]+', det_devices)        
            list_args = [det_list]     
            number = int(plan_dict["num"]['value'])  
            delay =  float(plan_dict["delay"]['value'])  

            item = {'item_type': 'plan',
                        'name': command,
                        'args': list_args,
                        'kwargs': {'num':number, 'delay': delay,'md':md}
                    }
  
        elif command == "grid_scan":

            det_devices = plan_dict["detectors~"]['value']
            det_list = re.findall(r'[^,;\s]+', det_devices)
            I0_string = plan_dict["I0~"]['value']
            I0_list=re.findall(r'[^,;\s]+', I0_string)
            det_list = det_list + I0_list
            xmot_name = plan_dict["x_motor"]['value']
            xstart = float(plan_dict["x_start"]['value'])
            xstop =float(plan_dict["x_stop"]['value'])
            xnumber = int(plan_dict["x_num"]['value'])
            ymot_name = plan_dict["y_motor"]['value']
            ystart = float(plan_dict["y_start"]['value'])
            ystop =float(plan_dict["y_stop"]['value'])
            ynumber = int(plan_dict["y_num"]['value'])
            list_args = [det_list, xmot_name, xstart, xstop, xnumber,ymot_name, ystart, ystop, ynumber]         
            snake_axes = eval(plan_dict["snaking"]['value'])
            md["I0"] = [s+ ".name" for s in I0_list] 

            item = {'item_type': 'plan',
                        'name': command,
                        'args': list_args,
                        'kwargs': {"snake_axes" :snake_axes, 'md':md}
                    }
            
        elif command == "move":

            mot_name = plan_dict["motor"]['value']
            position = float(plan_dict["position"]['value'])
            relative = eval(plan_dict["relative"]['value'])

            name = "mv"
            if relative:
                name = "mvr"
          
            list_args = [mot_name,position]         

            item = {'item_type': 'plan',
                        'name': name,
                        'args': list_args,
                        'kwargs': {}
                    }

        return item
            
    
    def createTable(self):
        """
        Create table (with plan parameters and default values)
        """
        _default_parameters_dict = self.default_parameters_dict
        self.param_dict = _default_parameters_dict.get(self.currently_selected_plan)
        #print(self.param_dict)
        self.insert_motornumber = 0
        self._tableWidget.setRowCount(len(self.param_dict))
        self._tableWidget.setColumnCount(2)
        self._tableWidget.horizontalHeader().setStretchLastSection(True)
        self._tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self._tableWidget.setHorizontalHeaderLabels(('Parameters', 'Value'))
        
        self._tableWidget.clear()
        self._tableWidget.setRowCount(len(self.param_dict))
        self._tableWidget.setColumnCount(2)
        self._tableWidget.horizontalHeader().setStretchLastSection(True)
        self._tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self._tableWidget.setHorizontalHeaderLabels(('Parameters', 'Value'))
        i=0
        if self.currently_selected_plan != "xas":
            for key, item in self.param_dict.items():

                param_name_item = QTableWidgetItem(key)
                self._tableWidget.setItem(i, 0, param_name_item)

                

                param_value_item = QTableWidgetItem(str(item))
                self._tableWidget.setItem(i, 1, param_value_item )
                param_value_item.setToolTip("Test tool Tip")
            


                            
                i = i+1

        
        elif self.currently_selected_plan == "xas":
            #print("Choose xas from custom plan.")
            i=0
            for key, item in self.param_dict.items():
                item = self._tableWidget.setItem(i, 0, QTableWidgetItem(key))
                #self._tableWidget.setItem(i, 1, QTableWidgetItem(str(item)))
                if key in ["I0","TEY", "TFY"]:

                    detector_lineEdit = QLineEdit()
                    completer = QCompleter(self.detectors_list)
                    detector_lineEdit.setCompleter(completer)
                    self._tableWidget.setCellWidget(i, 1, detector_lineEdit)
                    self._tableWidget.setItem
                i=i+1


        self._pb_cancel.setEnabled(True)
        self._pb_add_to_queue.setEnabled(True)


    def getValueList(self):
        """
        Get the user input of the plan from the table, all input must be filled. The default "None" value can be
        still "None".
        """
        dict_item = {}
        if self.currently_selected_plan != "xas":
            parameters_number = self._tableWidget.rowCount()
            for i in range(0, parameters_number):
                if self._tableWidget.cellWidget(i,1):
                    dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.cellWidget(i, 1).text()
                else:
                    dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.item(i, 1).text()
            #print(dict_item)
        else:
            parameters_number = self._tableWidget.rowCount()
            for i in range(0, parameters_number):
                if i in [0, 1, 2, 3, 7, 8]:
                    dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.cellWidget(i, 1).currentText()
                elif i in [4, 5, 6]:
                    input_result = self._tableWidget.item(i, 1).text()
                    if self.is_num(input_result):
                        if float(self._tableWidget.item(4, 1).text()) == float(self._tableWidget.item(5, 1).text()):
                            self.lineEdit.setText("Start Energy cannot be equal to stop energy.")
                        else:
                            dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.item(i, 1).text()
                    else:
                        self.lineEdit.setText("Input values are not valid")
                else:
                    dict_item[self._tableWidget.item(i, 0).text()] = self._tableWidget.item(i, 1).text()
        return dict_item

    def is_num(self, str):
        """
        Check if the string can be converted into numbers.
        """
        try:
            return float(str)
        except ValueError:
            return False

    def klick_addMotors(self):
        """
        Add widgets of more motors for plan "scan", "mv", "mvr".
        """
        if self.currently_selected_plan in ["scan"]:
            self.addscanMotors()
        elif self.currently_selected_plan in ["mv", "mvr"]:
            self.addmvMotors()
        else:
            self.addMotor_button.setEnabled(False)

    def addmvMotors(self):
        """
        Add more motors and set_points in mv plan.
        """
        add_parameter_list = ["new mot " + str(self.insert_motornumber),
                              "new set_point " + str(self.insert_motornumber)]
        rowCount = self._tableWidget.rowCount()
        for i in range(len(add_parameter_list)):
            self._tableWidget.insertRow(rowCount)
            self.param_list.insert(rowCount + i + 1, add_parameter_list[i])
        self.insert_motornumber += 1
        for i in range(len(self.param_list)):
            parameter = self.param_list[i]
            self._tableWidget.setItem(i, 0, QTableWidgetItem(parameter))

    def addscanMotors(self):
        """
        Add more motors, start positions, stop positions in the scan plan.
        """
        add_parameter_list = ["new mot " + str(self.insert_motornumber), "new start " + str(self.insert_motornumber),
                              "new stop " + str(self.insert_motornumber)]
        rowCount = self._tableWidget.rowCount()
        for i in range(len(add_parameter_list)):
            self._tableWidget.insertRow(rowCount)
            self.param_list.insert(rowCount + i + 1, add_parameter_list[i])

        self.insert_motornumber += 1
        for i in range(len(self.param_list)):
            parameter = self.param_list[i]
            self._tableWidget.setItem(i, 0, QTableWidgetItem(parameter))


class QtRePlanEditor(QWidget):
    signal_update_widgets = Signal()
    signal_running_item_changed = Signal(object, object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model.run_engine
        self._plan_viewer = _QtReViewer(self.model)
        self._plan_editor = _QtReEditor(self.model)
        self._add_plan = AddCustomPlanWidget(self.model)
        self._tab_widget = QTabWidget()
        self._tab_widget.addTab(self._add_plan, "Add Plan")
        self._tab_widget.addTab(self._plan_viewer, "Plan Viewer")
        self._tab_widget.addTab(self._plan_editor, "Plan Editor")
     

        vbox = QVBoxLayout()
        vbox.addWidget(self._tab_widget)
        self.setLayout(vbox)

        self._plan_viewer.signal_edit_queue_item.connect(self.edit_queue_item)
        self._plan_editor.signal_switch_tab.connect(self._switch_tab)

    @Slot(str)
    def _switch_tab(self, tab):
        tabs = {"view": self._plan_viewer, "edit": self._plan_editor}
        self._tab_widget.setCurrentWidget(tabs[tab])

    @Slot(object)
    def edit_queue_item(self, queue_item):
        self._switch_tab("edit")
        self._plan_editor.edit_queue_item(queue_item)

