import json

from qtpy.QtWidgets import (
    QWidget,
    QLabel,
    QPushButton,
    QToolButton,
    QVBoxLayout,
    QHBoxLayout,
    QFileDialog,
    QLineEdit,
    QGroupBox,
    QGridLayout,
    QDialog,
    QListWidget,
    QFrame,
    QComboBox,
    QTableWidget,
    QTableWidgetItem,
    QAbstractScrollArea,
    QTabWidget,
    QScrollArea,
    QTreeView,
    QSizePolicy,
    QFrame,
    QDoubleSpinBox,
    QCheckBox,
    QHeaderView,
    QAbstractItemView,
    QApplication,
    QMessageBox,
    QTextEdit
)

from bluesky_widgets.qt.threading import *
import Levenshtein as lev

from bluesky_queueserver.manager.conversions import spreadsheet_to_plan_list
from bluesky_queueserver_api import BFunc
from bluesky_queueserver_api.zmq import REManagerAPI   

import os, time
import socket

from qtpy.QtCore import Qt, Signal, Slot, QTimer, QPoint
from qtpy.QtGui import QStandardItemModel, QStandardItem, QDoubleValidator, QPalette, QIntValidator, QIcon

from .plans import plans

from .plan_custom import CollapsibleBox

from bluesky_widgets.qt.run_engine_client import _QtRePlanEditorTable, _QtReViewer, _QtReEditor

import itertools

class QtRePlanEditor(QWidget):
    signal_update_widgets = Signal()
    signal_running_item_changed = Signal(object, object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model.run_engine
        self._add_plan = QtCreateNewPlan(self.model)
        self._metadata = QtAddMetadata(self.model)
        # self._file_config = QtFileConfig(self.model)
        self._data_structure = QtDataStructure(self.model)
        self.setMinimumSize(500, 500)
        self._tab_widget = QTabWidget()
        self._tab_widget.addTab(self._add_plan, "Add Plan")
        self._tab_widget.addTab(self._metadata, "Metadata")
        # self._tab_widget.addTab(self._file_config, "File Config")
        self._tab_widget.addTab(self._data_structure, "Data structure")

        vbox = QVBoxLayout()
        vbox.addWidget(self._tab_widget)
        self.setLayout(vbox)

        self.model.events.queue_item_edit.connect(self._plan_editor_cb)

    @Slot(str)
    def _switch_tab(self, tab):
        tabs = {"view": self._plan_viewer, "edit": self._plan_editor}
        self._tab_widget.setCurrentWidget(tabs[tab])

    @Slot(object)
    def edit_queue_item(self, queue_item):
        self._switch_tab("edit")
        self._plan_editor.edit_queue_item(queue_item)

    def _plan_editor_cb(self, event):
        self.model.editing = True
        self._add_plan.signal_edit_item.emit(event)

class QtAddMetadata(QTableWidget):
    signal_update_widgets = Signal(object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widgets.connect(self.slot_update_widgets)

        ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
        self._delete_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/delete.png'))

        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        self.top_label = QLabel('Add Metadata')
        hbox.addWidget(self.top_label)
        hbox.addStretch()
        self.name = QLineEdit(self)
        self.name.setMinimumWidth(200)
        self.name.setPlaceholderText('Name for the Metadata')
        self.name.textChanged.connect(self.disable_add)
        self.name.returnPressed.connect(self.return_fnc)
        self.name.setEnabled(False)
        hbox.addWidget(self.name)
        self.add_btn = QPushButton('+')
        self.add_btn.clicked.connect(self.add_input)
        self.add_btn.setEnabled(False)
        hbox.addWidget(self.add_btn)
        vbox.addLayout(hbox)

        self.input_fields = {}

        self.tree = QTreeView()
        self.tree_model = QStandardItemModel()
        self.tree_model.setHorizontalHeaderLabels(['Name', 'Input', ''])
        self.tree.setModel(self.tree_model)
        self.tree.header().setSectionResizeMode(0, QHeaderView.ResizeMode.Stretch)
        self.tree.header().setSectionResizeMode(1, QHeaderView.ResizeMode.Stretch)
        self.tree.header().setSectionResizeMode(2, QHeaderView.ResizeMode.ResizeToContents)
        self.tree.header().setStretchLastSection(False)
        vbox.addWidget(self.tree)

        self.setLayout(vbox)

    def on_update_widgets(self, event):
        self.signal_update_widgets.emit(event)

    @Slot(object)
    def slot_update_widgets(self, event):
        self._update_widgets()
    
    def _update_widgets(self):
        is_connected = bool(self.model.re_manager_connected)
        self.name.setEnabled(is_connected)
    
    def disable_add(self):
        activate = len(self.name.text().strip()) > 0
        self.add_btn.setEnabled(activate)

    def return_fnc(self):
        if len(self.name.text().strip()) > 0:
            self.add_input()
    
    def add_input(self):
        name = self.name.text()
        self.name.clear()

        if not self.model.add_to_md(name):
            print('MD Key already in use!')
            return
        
        #Add input
        child = QStandardItem(name)
        child_in = QStandardItem('')
        child_rm = QStandardItem('')
        self.tree_model.invisibleRootItem().appendRow([child, child_in, child_rm])
        self.input_fields[name] = QtTextInput(name)
        self.input_fields[name].field.editingFinished.connect(
            lambda: self.model.update_md(name, self.input_fields[name].get_value())
        )
        self.tree.setIndexWidget(child_in.index(), self.input_fields[name])

        def rm_item(name, row):
            self.model.remove_from_md(name)
            self.tree_model.invisibleRootItem().removeRow(row)
            del self.input_fields[name]

        rm_btn = QPushButton()
        rm_btn.setIcon(self._delete_icon)
        rm_btn.setFixedSize(25,25)
        rm_btn.clicked.connect(lambda x: rm_item(name, child_in.index().row()))
        _widget = QWidget()
        box = QVBoxLayout()
        box.addWidget(rm_btn)
        _widget.setLayout(box)
        self.tree.setIndexWidget(child_rm.index(), _widget)
        

class QtFileConfig(QTableWidget):


    signal_update_widgets = Signal(object)
    
    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self.experiment_config_path="/opt/bluesky/bluesky/beamlinetools/beamlinetools/beamline_config/experiment_config.json"

        vbox = QVBoxLayout()
        vbox_fields = QVBoxLayout()

        with open(self.experiment_config_path, 'r') as f:
            config_data = json.load(f)

        for group_name, group_data in config_data.items():
            group_label = QLabel('<b>' + group_name + '</b>')
            vbox_fields.addWidget(group_label) 
            for field_name, field_value in group_data.items():
                hbox_field = QHBoxLayout()
                field_label = QLabel(field_name)
                field_input = QLineEdit()
                field_input.setText(field_value)
                hbox_field.addWidget(field_label)
                hbox_field.addWidget(field_input)

                if field_name == 'data_path':
                    field_button = QPushButton('...')
                    field_button.clicked.connect(lambda _, field_input=field_input: self.open_file_dialog(field_input))
                    hbox_field.addWidget(field_button)

                vbox_fields.addLayout(hbox_field)
            vbox_fields.addSpacing(100) 


        vbox.addLayout(vbox_fields)
        
        hbox_buttons = QHBoxLayout()

        save_button = QPushButton('Save')
        save_button.clicked.connect(lambda: self.save_config(vbox_fields, config_data))
        default_button = QPushButton('Default/ICAT?')
        hbox_buttons.addWidget(save_button)
        hbox_buttons.addWidget(default_button)

        ## idea: add a button to fill with ICAT data, if logged in ??
        ## maybe add to metadata instead of ICAT data, when  they are not aviablable?

        vbox.addLayout(hbox_buttons)

        self.setLayout(vbox)

    def open_file_dialog(self, path_field):

        file_dialog = QFileDialog(self)
        file_dialog.setFileMode(QFileDialog.Directory)
        if file_dialog.exec_():
            file_path = file_dialog.selectedFiles()[0]
            path_field.setText(file_path)

    def save_config(self,vbox_fields, config_data):
        
        print("start saving...")
        for i in range(vbox_fields.count()):
            item = vbox_fields.itemAt(i)
            if isinstance(item, QHBoxLayout):
                for j in range(item.count()):
                    sub_item = item.itemAt(j).widget()
                    if isinstance(sub_item, QLabel):
                        print(sub_item.text())
                    if isinstance(sub_item, QLineEdit):
                        print(sub_item.text())
            else:
                print(isinstance(item))
                if isinstance(item, QLabel):
                    print("group...")
                    print(item.itemAt(i).widget().text())


        # for group_name, group_data in config_data.items():
        #     for field_name, field_value in group_data.items():
        #         field_input = self.findChild(QLineEdit, field_name)

        #         if field_input:
        #             config_data[group_name][field_name] = 'test' #field_input.value()
        #     print(field_input)
        # print(config_data)

        # with open(self.experiment_config_path, 'w') as f:
        #     json.dump(config_data, f, indent=4)


class QtDataStructure(QTableWidget):
    
    signal_update_widgets = Signal(object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model

        self.re_status = None  # Initialize the re_status variable
        self.model.events.status_changed.connect(self.update_re_status)

        self.bluesky_data_path = "/opt/bluesky/data"
        
        # raise an error when bluesky data dir is not mounted
        if not os.path.exists(self.bluesky_data_path):
            print("\n" + "="*80)
            print("ERROR: Data directory not accessible!")
            print(f"The directory '{self.bluesky_data_path}' is not accessible.")
            print("\nPlease start the container with:")
            print("-v /home/username/bluesky/data:/opt/bluesky/data")
            print("="*80 + "\n")
            raise FileNotFoundError(f"Cannot access {self.bluesky_data_path}. Container must be started with proper volume mounting.")

        # self.initiate_re_status()

        if self.re_status!=None:
            self.experiment_set=self.read_from_re_md('exp_name')
            self.data_set=self.read_from_re_md('base_name')
            self.current_scan_id=self.read_from_re_md("scan_id")
        else:
            self.experiment_set="environment closed"
            self.data_set="environment closed"
            self.current_scan_id="environment closed"

        # myspot eiger
        self._eiger_exp_dir = "/user/mySpot2"

        vbox = QVBoxLayout()
        vbox_fields = QVBoxLayout()

        group_label = QLabel('<b> Data structure</b>')
        vbox_fields.addWidget(group_label)

        # summary of all experiments
        hbox_preview_labels_exp =QHBoxLayout()
        exp_dirs_label = QLabel('existing experiments:')
        exp_data_priview_lable = QLabel('Data preview:')
        hbox_preview_labels_exp.addWidget(exp_dirs_label)
        hbox_preview_labels_exp.addWidget(exp_data_priview_lable)
        vbox_fields.addLayout(hbox_preview_labels_exp)        

        hbox_preview_exp =QHBoxLayout()
        
        self.exp_dirs_list = QListWidget()
        self.exp_dirs_list.setSelectionMode(QAbstractItemView.SingleSelection)

        exp_dirs = os.listdir(self.bluesky_data_path)
        self.exp_dirs_list.addItems(sorted(exp_dirs))

        self.exp_dirs_list.itemClicked.connect(lambda item: self.exp_name_input.setText(item.text()))
        
        hbox_preview_exp.addWidget(self.exp_dirs_list)

        self.data_dirs_preview_list = QListWidget()
        self.data_dirs_preview_list.setSelectionMode(QAbstractItemView.SingleSelection)
        self.data_dirs_preview_list.itemClicked.connect(lambda item: self.sample_name_input.setText(item.text()))
        hbox_preview_exp.addWidget(self.data_dirs_preview_list)

        vbox_fields.addLayout(hbox_preview_exp)

        # fields for new experiment 
        hbox_exp = QHBoxLayout()
        exp_name_label = QLabel('<span style="color: #FF0000; font-weight: bold;">new experiment</span>')
        self.exp_name_input = QLineEdit()
        self.exp_name_save_button = QPushButton('set')
        self.exp_name_input.setPlaceholderText('new experiment name')
        self.exp_name_save_button.clicked.connect(lambda: self.creat_exp_directory(self.exp_name_input.text()))
        self.exp_name_save_button.setEnabled(self.re_status == 'idle')
        self.exp_name_input.textChanged.connect(self.update_buttons_status)
        self.exp_name_input.textChanged.connect(self.check_if_exp_dir_exists)

        hbox_exp.addWidget(exp_name_label)
        hbox_exp.addWidget(self.exp_name_input)

        vbox_fields.addLayout(hbox_exp)

        # fields for new data directory
        hbox_sample = QHBoxLayout()
        sample_name_label = QLabel('<span style="color: #0000FF; font-weight: bold;">newdata</span>')
        self.sample_name_input = QLineEdit()
        self.sample_name_save_button = QPushButton('set/create')
        self.sample_name_save_button.clicked.connect(self.handle_sample_name_save)
        self.sample_name_save_button.setEnabled(self.re_status == 'idle')
        self.sample_name_input.setPlaceholderText(" new data dir name")
        self.sample_name_input.textChanged.connect(self.update_buttons_status) 

        hbox_sample.addWidget(sample_name_label)
        hbox_sample.addWidget(self.sample_name_input)
        hbox_sample.addWidget(self.sample_name_save_button)

        self.data_dirs_set_label = QLabel('<b>Current folder structure: </b>') 
        self.current_scan_id_label = QLabel('Scan_id: ' + str(self.current_scan_id))
        hbox_data_set = QHBoxLayout()

        vbox_data_dirs = QVBoxLayout()
        self.data_dirs_label = QLabel()
        self.data_dirs_label.setText(self.update_datastructure_label())

        self.status_label = QLabel()
        self.status_label.setStyleSheet("background-color: lightgrey; padding: 5px;")
        self.status_label.setText("") 

        vbox_data_dirs.addWidget(self.current_scan_id_label)
        vbox_data_dirs.addWidget(self.data_dirs_set_label)
        
        vbox_data_dirs.addWidget(self.data_dirs_label)
        

        vbox_fields.addLayout(hbox_sample)
        vbox_fields.addLayout(hbox_data_set)
        
        vbox_fields.addWidget(self.status_label) 

        vbox_fields.addSpacing(10)

        vbox_fields.addLayout(vbox_data_dirs)
        
        vbox.addLayout(vbox_fields)

        self.setLayout(vbox)

        self.exp_dirs_list.itemClicked.connect(self.update_data_dirs_list)
               
    # creates the string for the datasructure label
    def update_datastructure_label(self):
        label_text = (
            '<pre>'
            ' <span style="color: #2E8B57; font-weight: bold;"> .../bluesky/data/ </span>\n'
            '    └── <span style="color: #FF0000; font-weight: bold;">' + self.experiment_set + '/' + '</span>\n'
            '        └── <span style="color: #0000FF; font-weight: bold;">' + self.data_set + '/' + '</span>\n'
            '            ├── <span style="color: #808080;">' + str(self.current_scan_id).zfill(5) + '_example.csv</span>\n'
            '            └── <span style="color: #808080;">' + str(self.current_scan_id).zfill(5) + '_example.spec</span>\n'
            '</pre>'
        )
        return label_text

    def check_if_exp_dir_exists(self):  
        
        exp_dirs = os.listdir(self.bluesky_data_path)
        if self.exp_name_input.text() in exp_dirs:
            data_directory_path = os.path.join(self.bluesky_data_path, self.exp_name_input.text())
            if os.path.isdir(data_directory_path):
                data_dirs = os.listdir(data_directory_path)
                self.data_dirs_preview_list.clear()
                self.data_dirs_preview_list.addItems(sorted(data_dirs))
        else:
            self.data_dirs_preview_list.clear()

    @Slot(object)
    def update_re_status(self, event):
        try:
            old_re_status = self.re_status
            self.re_status = event.status.get("re_state", None)
            self.update_buttons_status() 

            is_connected = bool(event.is_connected)
            if old_re_status is None and is_connected:
                self.experiment_set = self.read_from_re_md('exp_name')
                self.data_set = self.read_from_re_md('base_name')
                self.current_scan_id = self.read_from_re_md("scan_id")
                self.current_scan_id_label.setText('Scan_id: ' + str(self.current_scan_id))
                self.data_dirs_label.setText(self.update_datastructure_label())
        except Exception as e:
            print(f"Exception in update_re_status: {e}")

    def initiate_re_status(self):
        re_manager = REManagerAPI()
        self.re_status=re_manager.status()['re_state']


    def update_buttons_status(self):
        if self.re_status == 'idle':
            if len(self.exp_name_input.text()) > 0:
                self.exp_name_save_button.setEnabled(True)
            else:
                self.exp_name_save_button.setEnabled(False)
            if len(self.sample_name_input.text()) > 0:
                self.sample_name_save_button.setEnabled(True)
            else:
                self.sample_name_save_button.setEnabled(False)
        else:
            self.exp_name_save_button.setEnabled(False)
            self.sample_name_save_button.setEnabled(False)

    def update_data_dirs_list(self, item):
        # This method will update data_dirs_list based on the clicked item in exp_dirs_list
        selected_dir = item.text()
        data_directory_path = os.path.join(self.bluesky_data_path, selected_dir)
        if os.path.isdir(data_directory_path):
            data_dirs = os.listdir(data_directory_path)
            self.data_dirs_preview_list.clear()
            self.data_dirs_preview_list.addItems(sorted(data_dirs))

    # add metadata directly to RE.md
    def add_to_re_md(self, key, value):
        
        re_manager = REManagerAPI()
        function = BFunc("qs_write_to_re_md",key=key, value=value)
        re_manager.function_execute(function)
        re_manager.wait_for_idle()
    
    # read metadata directly from RE.md
    def read_from_re_md(self, key):
        
        re_manager = REManagerAPI()
        function = BFunc("qs_read_from_re_md",key=key)
        # md_entry=re_manager.function_execute(function)['item']['kwargs']['key']
        res=re_manager.function_execute(function)
        re_manager.wait_for_idle()
        
        task_uid=res['task_uid']
        task_res=re_manager.task_result(task_uid=task_uid)
        md_res=task_res['result']['return_value']

        return md_res

    def creat_exp_directory(self, exp_name):
        
        if exp_name[:3] == "202" and exp_name[10]=="_":
            self._exp_name = exp_name
        else:
            self._exp_name = time.strftime('%Y-%m-%d')+'_'+exp_name
        self.add_to_re_md("exp_name",self._exp_name)
       
        self._experiment_dir = os.path.join(self.bluesky_data_path,self._exp_name)
        
        self.add_to_re_md("experiment_dir", self._experiment_dir)

        # Check if the directory exists
        if not os.path.exists(self._experiment_dir):
            # If it doesn't exist, create it
            os.makedirs(self._experiment_dir)

        
        # self._set_number = 0
        # self.add_to_re_md("set_number", self._set_number)
             
        #update exp_dirs
        self.exp_dirs_list.clear()
        self.exp_dirs_list.addItems(sorted(os.listdir(self.bluesky_data_path)))
        
    def set_md_paths(self):
        re_manager = REManagerAPI()
        function = BFunc("qs_set_md_paths")
        re_manager.function_execute(function)
        re_manager.wait_for_idle()

    def set_spec_factory_prefixes(self, base_name, newdata_dir):
        re_manager = REManagerAPI()
        function = BFunc("qs_set_spec_factory_prefixes",base_name=base_name, newdata_dir=newdata_dir)
        re_manager.function_execute(function)
        re_manager.wait_for_idle()

    def set_csv_export_folder(self,path):   
        re_manager = REManagerAPI()
        function = BFunc("qs_set_csv_export_folder",path)
        re_manager.function_execute(function)
        re_manager.wait_for_idle()

    def create_new_data_directory(self, base_name, new_experimentname):

        _exp_name = self.read_from_re_md("exp_name")
  

        _experiment_dir=self.read_from_re_md("experiment_dir")


        # update self variables
        if base_name[:3] == "202" and base_name[10]=="_":
            _base_name = base_name
        else:
            Ymd = time.strftime('%Y-%m-%d')
            # _base_name = Ymd+'_set'+"{:03d}".format(_set_number)+'_'+base_name  
            _base_name = Ymd+'_'+base_name   
        
        _newdata_dir = os.path.join(_experiment_dir,_base_name)

        if not os.path.exists(_newdata_dir):
            # If it doesn't exist, create it       
            os.makedirs(_newdata_dir)
            print(f'Created new folder {_newdata_dir}')
            
            self.current_scan_id = 0
            self.add_to_re_md("scan_id",self.current_scan_id)    

        # Update spec factory prefixes
        self.update_status_label("Setting spec factory prefixes...")
        QApplication.processEvents()
        self.set_spec_factory_prefixes(_base_name, _newdata_dir)

        self.update_status_label("Update RE.md...")
        QApplication.processEvents()
        # Update RE.md with new values
        # self.add_to_re_md("set_number", _set_number)
        self.add_to_re_md("base_name", _base_name)
        # move from here, it not belongs here
        self.add_to_re_md("base_eiger", os.path.join(self._eiger_exp_dir, _exp_name, _base_name))
        self.add_to_re_md("specfile", os.path.join(_newdata_dir))
        self.add_to_re_md("hostname", socket.gethostname()) 
        self.add_to_re_md("exported_data_dir", _newdata_dir)

        self.experiment_set = _exp_name
        self.data_set = _base_name
        
        # Update the data_dirs_label here
        self.update_status_label("Update data structure preview...")
        self.data_dirs_label.setText(self.update_datastructure_label())
        

    def update_status_label(self, text="", status="normal"):
        if status == "success":
            # Add a green check mark for success
            check_mark = '<span style="color: green;">&#10003;</span>'  # Unicode for check mark
            self.status_label.setText(check_mark + " " + text)
        elif status == "fail":
            # Add a red cross for failure
            cross_mark = '<span style="color: red;">&#10006;</span>'  # Unicode for red cross (✖)
            self.status_label.setText(cross_mark + " " + text)
        else:
            # Normal status (no special mark)
            self.status_label.setText(text)

    def check_if_data_dir_exists(self):
        if os.path.exists(self.bluesky_data_path + '/' + self.exp_name_input.text()):
            list_dataest_in_exp_dir = os.listdir(self.bluesky_data_path + '/' + self.exp_name_input.text())
            
            if self.sample_name_input.text() in list_dataest_in_exp_dir:

                return True
            else:
                return False
        else:
            return False

    def handle_sample_name_save(self):
        # Check if the data dir already exists in the experiment dir
        
        if "commission" in self.exp_name_input.text():
            
            self.update_status_label("Setting spec factory prefixes...")
            QApplication.processEvents()
            self.set_spec_factory_prefixes("commssioning_file", os.path.join(self.bluesky_data_path,"2024_commissioning"))

            self.update_status_label("Setting exporting folder for...")
            QApplication.processEvents()
            self.set_csv_export_folder(os.path.join(self.bluesky_data_path,"2024_commissioning"))

            self.update_status_label("Change to commissioning directory.","success")
        else:

            if self.check_if_data_dir_exists():
            # Show a confirmation dialog
            
                QMessageBox.information(self, 'Directory Exists',
                            "The data directory already exists.", 
                            QMessageBox.Ok)
            
                return

            else:

                # Update the status label to indicate the action is in progress
                self.update_status_label("Checking/creating experiment directory...")
                QApplication.processEvents()  

                self.creat_exp_directory(self.exp_name_input.text())
                
                self.update_status_label("Checking/creating data directory...")
                QApplication.processEvents() 

                self.create_new_data_directory(self.sample_name_input.text(), self.exp_name_input.text())

                self.current_scan_id_label.setText('Scan_id: ' + str(self.current_scan_id))

                # set csv export folder
                self.set_csv_export_folder(os.path.join(self.bluesky_data_path,self.exp_name_input.text(),self.sample_name_input.text()))

                # Tell the RE to update any relevant paths
                self.set_md_paths()
            
                # Update the status label to indicate completion   
                self.update_status_label("New data directory set/created successfully!","success")
             

class QtCreateNewPlan(QTableWidget):
    signal_update_widgets = Signal(bool)
    #signal_switch_tab = Signal(str)
    signal_allowed_plan_changed = Signal(object)
    signal_allowed_devices_changed = Signal(object)
    signal_update_list_of_devices = Signal(object)
    signal_edit_item = Signal(object)

    signal_selection_changed = Signal(object, object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widgets.connect(self.slot_update_widgets)
        self.signal_selection_changed.connect(self.slot_selection_changed)

        self.model.events.allowed_devices_changed.connect(self._on_allowed_devices_changed)
        self.signal_allowed_devices_changed.connect(self._slot_allowed_devices_changed)
        self.model.events.allowed_plans_changed.connect(self._on_allowed_plan_changed)
        self.signal_allowed_plan_changed.connect(self._slot_allowed_plan_changed)
        self.signal_edit_item.connect(self._slot_edit_item)

        self._allowed_devices = {}
        self._allowed_plans = {}
        
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        self.select_plan = QComboBox(self)
        self.select_plan.addItem(None)
        self.select_plan.setSizeAdjustPolicy(0)
        self.select_plan.currentTextChanged.connect(self._select_plan)
        hbox.addWidget(self.select_plan)
        hbox.addStretch()
        self.clear = QPushButton('Clear')
        self.clear.clicked.connect(self._clear_view)
        self.submit = QPushButton('Submit')
        self.submit.clicked.connect(self.submit_plan)
        hbox.addWidget(self.clear)
        hbox.addWidget(self.submit)
        vbox.addLayout(hbox)

        self.input_fields = {}

        self.tree = QTreeView()
        self.tree_model = QStandardItemModel()
        self.tree_model.setHorizontalHeaderLabels(['Name', 'Input'])
        self.tree.header().setDefaultSectionSize(180)
        self.tree.setModel(self.tree_model)
        vbox.addWidget(self.tree)
        #vbox.addStretch()

        # Add docstring text display to add below the tree
        self.doc_text = QTextEdit()
        self.doc_text.setReadOnly(True)
        self.doc_text.setMinimumHeight(50)
        self.doc_text.setMaximumHeight(100)
        self.doc_text.hide() 
        vbox.addWidget(self.doc_text)
        
        self.setLayout(vbox)

    def on_update_widgets(self, event):
        # None should be converted to False:
        is_connected = bool(event.is_connected)
        self.signal_update_widgets.emit(is_connected)

    @Slot(object)
    def _slot_edit_item(self, event):
        a = self.select_plan.findText(event.item['name'])
        self.select_plan.setCurrentIndex(a)
        for arg, val in event.item['kwargs'].items():
            if arg == 'md':
                self.input_fields[arg].set_value(str(val['md']))
            else:
                self.input_fields[arg].set_value(val)
    
    @Slot()
    def slot_update_widgets(self):
        self._update_widget_state()

    def _update_widget_state(self):

        is_connected = bool(self.model.re_manager_connected)

        #self._rb_item_plan.setEnabled(not self._edit_mode_enabled)
        #self._rb_item_instruction.setEnabled(not self._edit_mode_enabled)
        self.select_plan.setEnabled(is_connected)

        #self._pb_add_to_queue.setEnabled(is_connected)
    
    def _on_allowed_devices_changed(self, event):
        self.signal_allowed_devices_changed.emit(event.allowed_devices)

    @Slot(object)
    def _slot_allowed_devices_changed(self, allowed_devices):
        self._allowed_devices = allowed_devices
        self.signal_update_list_of_devices.emit(allowed_devices)

    def _on_allowed_plan_changed(self, event):
        self.signal_allowed_plan_changed.emit(event.allowed_plans)

    @Slot(object)
    def _slot_allowed_plan_changed(self, allowed_plans):
        self._allowed_plans = allowed_plans
        self.select_plan.clear()
        self.select_plan.addItem(None)

        # Filter plans that either:
        # 1. Have no parameters
        # 2. Only have 'md' parameter
        # 3. Have annotations in their parameters (but not default bluesky plan annotations)
        filtered_plans = [
            plan_name for plan_name in allowed_plans 
            if (isinstance(allowed_plans[plan_name], dict) and 
                (len(allowed_plans[plan_name]['parameters']) == 0 or  # no parameters
                 (len(allowed_plans[plan_name]['parameters']) == 1 and  # only md parameter
                  allowed_plans[plan_name]['parameters'][0].get('name') == 'md') or
                 (any('annotation' in param for param in allowed_plans[plan_name]['parameters']) and  # has annotations 
                  allowed_plans[plan_name].get('module') != 'bluesky.plans')
                ))
        ]

        # Add plans to combobox with tooltips
        for plan_name in sorted(filtered_plans):
            plan_data = allowed_plans[plan_name]
            docstring = plan_data.get('description', '') 
            index = self.select_plan.count()  
            self.select_plan.addItem(plan_name) 
            if docstring:  
                self.select_plan.setItemData(index, docstring, Qt.ToolTipRole)

    @Slot(object, object)
    def slot_selection_changed(self, source, selection):
        for name, field in self.input_fields.items():
            try:
                is_dependant = (field.depends_on == source)
            except AttributeError as e:
                is_dependant = False
            if is_dependant:
                if selection == '':
                    field.set_field([])
                elif isinstance(selection, list):
                    print('List dependency not yet implemented!')
                else:
                    items = [i for i in field.selection_dict[selection]]
                    field.set_field(items)

    def submit_plan(self):
        # Check if the batch field have a content
        for name, field in self.input_fields.items():
            if isinstance(field, QtPlanInput) and field.content is None:
                QMessageBox.warning(self, 'Missing Input', 
                                  'Please select a batch file before submitting')
                return

        item = {
            'item_type': 'plan',
            'name': str(self.select_plan.currentText()),
            'args': {},
            'kwargs': {},
        }

        for name, field in self.input_fields.items():
            if not name == 'md':
                item['kwargs'][name] = field.get_value()
            else:
                item['kwargs']['md'] = {name: field.get_value()}

        try:
            self.model.queue_item_add(item=item)
            if self.model.editing:
                self.model.editing = False
                self.model.queue_items_move_up()
                
            #self._clear_view()
        except RuntimeError as e:
            print(e)

    def _clear_view(self):
        for name, field in self.input_fields.items():
            field.clear()

    def _select_plan(self, text):
        #remove current widgets
        self.tree_model.clear()
        self.input_fields = {}
        self.tree_model.setHorizontalHeaderLabels(['Name', 'Input'])
        self.doc_text.clear()
        self.doc_text.hide() 
        
        if not text:
            return
            
        self._fill_tree(self.tree_model.invisibleRootItem(), self._allowed_plans[text])

         # Add docstring as description below the tree
        if isinstance(self._allowed_plans[text], dict):
            # Try to get full description from annotations, fall back to description from plan itself
            try:
                docstring = self._allowed_plans[text].get('description', '')
            except Exception:
                docstring = None
                
            if docstring:
                
                self.doc_text.setStyleSheet("QTextEdit { white-space: pre-wrap; }")
                self.doc_text.insertHtml("<b>Plan documentation:</b><br>")
                self.doc_text.insertPlainText("\n" + docstring)
                self.doc_text.show()


    def _fill_tree(self, root, plan):
        for param in plan['parameters']:
            child = QStandardItem(param['name'])
            child_in = QStandardItem('')
            root.appendRow([child, child_in])
            self.input_fields[param['name']] = self._create_input(param)
            self.tree.setIndexWidget(child_in.index(), self.input_fields[param['name']])
    
    def _create_input(self, param):
        _name = param['name']
        if not 'annotation' in param.keys():
            return QtTextInput(name=_name)
        try:
            _type, _subtype = param['annotation']['type'].rsplit(']', 1)[0].split('[', 1)
            _subtype = _subtype.replace(' ', '')
        except ValueError as e:
            _type = param['annotation']['type']
            _subtype = ''
        if _type == 'typing.List' or _type == 'typing.Sequence':
            if _subtype == 'str':
                return QtDeviceInput(
                    name=_name,
                    items=self._allowed_devices,
                )
            if 'typing.Tuple' in _subtype:
                try:
                    tuple_types = _subtype.rsplit(']', 1)[0].split('[', 1)[1].split(',')
                except IndexError as e:
                    tuple_types = None

                try:
                    tuple_descr = param['description'].split(',')
                except KeyError as e:
                    tuple_descr = None

                return QtMotorInput(
                    name=_name,
                    t_types = tuple_types,
                    t_descr = tuple_descr,
                    items=self._allowed_devices,
                )

            if '__PLAN__' in _subtype:
                return QtPlanInput(
                    name=_name,
                )

            else:
                print('Subtype: ', _subtype)
        
        # this should create new field dependent on the choosen fields
        # BUG: GUI is crashing when default bluesky plan is selected
        # elif _type == 'typing.Union':
        #     depends_on = param['description']
        #     if 'enums' in param['annotation']:
        #         selection_dict = param['annotation']['enums']
        #     else:
        #         selection_dict = param['annotation']['devices']

        #     _numeric_dict = {
        #         'min': None,
        #         'max': None,
        #         'default': None,
        #         'step': None,
        #     }

        #     for key in _numeric_dict.keys():
        #         if key in param.keys():
        #             try:
        #                 _numeric_dict[key] = float(param[key])
        #             except ValueError as e:
        #                 pass

        #     device = QtUnionInput(
        #         name=_name,
        #         selection_dict=selection_dict,
        #         depends_on=depends_on,
        #         numeric_dict=_numeric_dict
        #     )
            
        #     def callback(name, selection):
        #         self.signal_selection_changed.emit(name, selection)

        #     device.add_callback(callback)

        #     return device
        
        elif 'enums' in param['annotation']:
            _name = next(iter(param['annotation']['enums']))
            device = QtEnumInput(
                name=_name,
                items=param['annotation']['enums'][_name],
            )

            def callback(name, selection):
                self.signal_selection_changed.emit(name, selection)

            device.add_callback(callback)

            return device

        elif _type == 'int' or _type == 'float':
            is_int = (_type == 'int')
            _numeric_dict = {
                'min': None,
                'max': None,
                'default': None,
                'step': None,
            }

            for key in _numeric_dict.keys():
                if key in param.keys():
                    try:
                        _numeric_dict[key] = float(param[key])
                    except ValueError as e:
                        pass

            return QtNumericInput(
                name=_name,
                is_int=is_int,
                **_numeric_dict,
            )
        elif _type == 'bool':
            _value = False
            if 'default' in param.keys():
                _value = eval(param['default'])
            return QtBoolInput(
                name=_name,
                value=_value
            )
        else:
            return QtTextInput(name=_name)

class QtTextInput(QWidget):
    def __init__(self, name, parent=None):
        super().__init__(parent)
        hbox = QHBoxLayout()
        self.name = name
        self.field = QLineEdit()
        hbox.addWidget(self.field)
        self.setLayout(hbox)

    def get_value(self):
        return self.field.text()

    def set_value(self, value):
        self.field.setText(value)
    
    def clear(self):
        self.field.clear()

class QtBoolInput(QWidget):
    def __init__(self, name, value=False, parent=None):
        super().__init__(parent)
        hbox = QHBoxLayout()
        self.name = name
        self.field = QCheckBox()
        self.field.setChecked(value)
        hbox.addWidget(self.field)
        self.setLayout(hbox)

    def get_value(self):
        return self.field.isChecked()

    def set_value(self, value):
        self.field.setChecked(value)
    
    def clear(self):
        self.field.setChecked(False)

class QtNumericInput(QWidget):
    def __init__(self, name, is_int, default, min, max, step, parent=None):
        super().__init__(parent)
        hbox = QHBoxLayout()
        self.name = name
        self.is_int = is_int
        self.field = QDoubleSpinBox()
        if is_int:
            self.field.setDecimals(0)
        else:
            self.field.setDecimals(6)

        if step:
            self.field.setSingleStep(step)
        if min:
            self.field.setMinimum(min)
        if max:
            self.field.setMaximum(max)
        if default:
            self.field.setValue(default)

        hbox.addWidget(self.field)
        self.setLayout(hbox)

    def get_value(self):
        try:
            if self.is_int:
                return int(self.field.value())
            else:
                return float(self.field.value())
        except ValueError as e:
            return 0
    
    def set_value(self, val):
        self.field.setValue(val)
    
    def clear(self):
        self.field.clear()

class QtUnionInput(QWidget):
    def __init__(self, name, numeric_dict, depends_on=None, selection_dict={}, parent=None):
        super().__init__(parent)
        self.name = name
        self.selection_dict = selection_dict
        self.depends_on = depends_on
        self.numeric_dict = numeric_dict
        self.callbacks = []
        self.box = QHBoxLayout()
        self.field = QtTextInput(name=self.name)
        self.field.setEnabled(False)
        self.box.addWidget(self.field)
        self.box.setContentsMargins(0,0,0,0)
        self.setLayout(self.box)

    def add_callback(self, callback):
        self.callbacks.append(callback)

    def selection_changed(self, name, selection):
        for callback in self.callbacks:
            callback(name, selection)
    
    def set_field(self, selection):
        self.setEnabled(True)
        if self.box.count() > 0:
            self.box.removeWidget(self.field)
        if len(selection) == 0:
            self.field = QtTextInput(name=self.name)
            self.box.addWidget(self.field)
            self.setEnabled(False)
        else:
            if 'widget:' in selection[0]:
                _type = selection[0].split(':')[1]
                if _type == 'float' or _type == 'int':
                    self.field = QtNumericInput(
                        name=self.name,
                        is_int=False,
                        **self.numeric_dict
                    )
                    self.box.addWidget(self.field)
                elif _type == 'None':
                    self.setEnabled(False)
                    self.field = QtTextInput(name=self.name)
                    self.box.addWidget(self.field)
                else:
                    print(_type)
            else:
                self.field = QtEnumInput(
                    name=self.name,
                    items=selection,
                )
                self.field.add_callback(self.selection_changed)
                self.box.addWidget(self.field)

    def get_value(self):
        val = self.field.get_value()
        if val == '':
            return None
        else:
            return val
    
    def set_value(self, val):
        self.field.set_value(val)
    
    def clear(self):
        self.field.clear()

class QtEnumInput(QWidget):
    def __init__(self, name, items, parent=None):
        super().__init__(parent)
        self.name = name
        self.items = items
        self.callbacks = []
        hbox = QHBoxLayout()
        self.field = QComboBox()
        if len(items) > 1:
            self.field.addItem('')
        self.field.addItems(items)
        self.field.currentTextChanged.connect(self.selection_changed)
        hbox.addWidget(self.field)
        self.setLayout(hbox)

    def set_items(self, devices):
        self.items = devices
        self.field.clear()
        self.field.addItem('')
        self.field.addItems(devices)

    def add_callback(self, callback):
        self.callbacks.append(callback)
        callback(self.name, self.field.currentText())

    def selection_changed(self, selection):
        for callback in self.callbacks:
            callback(self.name, selection)

    def get_value(self):
        return str(self.field.currentText())

    def set_value(self, val):
        index = self.field.findText(val, Qt.MatchFixedString)
        if index >= 0:
            self.field.setCurrentIndex(index)
            self.selection_changed(val)
    
    def clear(self):
        self.field.setCurrentIndex(0)

class QtMotorInput(QWidget):

    def __init__(self, name, t_types, t_descr, items, depends_on='', selection_dict={}, parent=None):
        super().__init__(parent)
        self.depends_on = depends_on
        self.name = name
        self.t_types = t_types
        self.t_descr = t_descr
        if self.t_descr is None:
            self.t_descr = []
        self.filter = []
        self.callbacks = []
        self.items = items
        self.selection_dict = selection_dict

        motor_ctrl_box = QHBoxLayout()
        self.inputs = []
        for name, typ in itertools.zip_longest(self.t_descr, self.t_types):
            if typ is None:
                break
            if typ == 'str' or typ == 'String':
                self.motor_select = QComboBox()
                if not name is None:
                    self.motor_select.setToolTip(name)
                motor_ctrl_box.addWidget(self.motor_select)
            elif typ == 'float':
                _spin_box = QDoubleSpinBox()
                if not name is None:
                    _spin_box.setToolTip(name)
                    _spin_box.setMinimum(-pow(10, 6))
                    _spin_box.setMaximum(pow(10, 6))
                    _spin_box.setSingleStep(0.5)
                self.inputs.append( (_spin_box, float) )
                motor_ctrl_box.addWidget(self.inputs[-1][0])
            elif typ == 'int':
                _spin_box = QDoubleSpinBox()
                if not name is None:
                    _spin_box.setToolTip(name)
                _spin_box.setDecimals(0)
                _spin_box.setMaximum(pow(10, 6))
                _spin_box.setSingleStep(1)
                self.inputs.append( (_spin_box, int) )
                motor_ctrl_box.addWidget(self.inputs[-1][0])
            else:
                print('TODO')

        self.add_btn = QToolButton()
        self.add_btn.setText('+')
        self.add_btn.setToolTip('Add')
        self.add_btn.clicked.connect(self.add_device)
        motor_ctrl_box.addStretch()
        motor_ctrl_box.addWidget(self.add_btn)

        self.motor_list = QtDeviceList()
        self.motor_list.signal_device_list_changed.connect(self.device_list_changed)
        vbox = QVBoxLayout()
        vbox.addLayout(motor_ctrl_box)
        vbox.addWidget(self.motor_list)
        self.setLayout(vbox)

        self.set_items(items)

    def get_selected_devices(self):
        return self.motor_list.devices

    def add_callback(self, callback):
        self.callbacks.append(callback)
        callback(self.motor_list.items)

    def device_list_changed(self):
        items = [i for i in self.items if i not in self.motor_list.devices]
        self.motor_select.clear()
        self.motor_select.addItem('')
        self.motor_select.addItems(items)

    def add_device(self, device_tuple=None):
        if not device_tuple:
            selection = self.motor_select.currentText()
            param = ''
            for dev, _ in self.inputs:
                param += ', ' + str(dev.value())
        else:
            selection = str(device_tuple[0])
            param = ''
            for i in device_tuple[1:]:
                param += ', ' + str(i)

        if not selection == '':
            self.motor_select.setCurrentIndex(0)
            for dev, _ in self.inputs:
                dev.clear()
            self.motor_list.add_device(selection+param)
            for callback in self.callbacks:
                callback(self.motor_list.devices)

    def set_items(self, devices):
        self.items = devices
        self.motor_select.clear()
        self.motor_list.clear()
        self.motor_select.addItem('')
        self.motor_select.addItems(devices)

    def get_value(self):
        _mtr_list = []
        for dev in self.motor_list.devices:
            item = dev.split(', ')
            element = (item[0],)
            for entry, (_, typ) in zip(item[1:], self.inputs):
                try:
                    element += (typ(entry),)
                except ValueError as e:
                    element += (typ(float(entry)),)
            _mtr_list.append( element )

        return _mtr_list

    def set_value(self, value):
        for val in value:
            self.add_device(val)

    def clear(self):
        self.motor_list.clear()
        for dev, _ in self.inputs:
            dev.clear()

class QtDeviceInput(QWidget):
    def __init__(self, name, items, depends_on='', selection_dict={}, parent=None):
        super().__init__(parent)
        self.depends_on = depends_on
        self.name = name
        self.filter = []
        self.callbacks = []
        self.items = items
        self.selection_dict = selection_dict
        self.add_device_btn = QComboBox()
        self.add_device_label = 'Add ' + self.name
        self.add_device_btn.currentTextChanged.connect(self.add_device)
        self.device_list = QtDeviceList()
        self.device_list.signal_device_list_changed.connect(self.device_list_changed)
        vbox = QVBoxLayout()
        vbox.addWidget(self.add_device_btn)
        vbox.addWidget(self.device_list)
        self.setLayout(vbox)

        self.set_items(items)

    def get_selected_devices(self):
        return self.device_list.devices

    def add_callback(self, callback):
        self.callbacks.append(callback)
        callback(self.device_list.items)

    def device_list_changed(self):
        items = [i for i in self.items if i not in self.device_list.devices]
        self.add_device_btn.clear()
        self.add_device_btn.addItem(self.add_device_label)
        self.add_device_btn.addItems(items)


    def add_device(self, selection):
        if not selection == self.add_device_label and not selection == '':
            self.add_device_btn.setCurrentIndex(0)
            self.device_list.add_device(selection)
            for callback in self.callbacks:
                callback(self.device_list.devices)

    def set_items(self, devices):
        self.items = devices
        self.add_device_btn.clear()
        self.device_list.clear()
        self.add_device_btn.addItem(self.add_device_label)
        self.add_device_btn.addItems(devices)

    def get_value(self):
        return self.device_list.devices

    def set_value(self, val):
        for dev in val:
            self.add_device(dev)

    def clear(self):
        self.device_list.clear()

class QtDeviceList(QTableWidget):

    signal_device_list_changed = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setColumnCount(1)
        self.setRowCount(0)
        self.devices = []
        self.horizontalHeader().setStretchLastSection(True)
        self.verticalHeader().hide()
        self.horizontalHeader().hide()
        self.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.itemDoubleClicked.connect(self.remove_device)

    def add_device(self, text):
        pos = len(self.devices)
        self.setRowCount(pos+1)
        item = QTableWidgetItem(text)
        item.setFlags(Qt.ItemIsEnabled)
        self.setItem(pos, 0, item)
        self.devices.append(text)
        self.resizeRowsToContents()
        self.signal_device_list_changed.emit()
    
    def remove_device(self, item):
        device = item.text()
        self.devices.remove(device)
        for row in range(self.rowCount()):
            if self.item(row, 0).text() == device:
                self.removeRow(row)
                break
        self.setRowCount(len(self.devices))
        self.signal_device_list_changed.emit()
    
    def clear(self):
        self.setRowCount(0)
        self.devices = []

class QtScrollLabel(QScrollArea):
 
    # constructor
    def __init__(self, *args, **kwargs):
        QScrollArea.__init__(self, *args, **kwargs)
 
        # making widget resizable
        self.setWidgetResizable(True)
 
        # making qwidget object
        content = QWidget(self)
        self.setWidget(content)
 
        # vertical box layout
        lay = QVBoxLayout(content)
 
        # creating label
        self.label = QLabel(content)
 
        # setting alignment to the text
        self.label.setAlignment(Qt.AlignLeft | Qt.AlignTop)
 
        # making label multi-line
        self.label.setWordWrap(True)
 
        # adding label to the layout
        lay.addWidget(self.label)
 
    # the setText method
    def setText(self, text):
        # setting text to the label
        self.label.setText(text)

class QtPlanInput(QWidget):
    def __init__(self, name, parent=None):
        super().__init__(parent)
        ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
        self.BATCH_DIR = os.environ.get('BATCH_SAVE_DIR', os.path.join(ROOT_DIR, 'batches/'))
        self.name = name
        vbox = QVBoxLayout()
        self.select_btn = QPushButton('Select Batch')
        self.select_btn.clicked.connect(self.select_batch)
        vbox.addWidget(self.select_btn)
        self.content = None
        self.content_label = QtScrollLabel()
        vbox.addWidget(self.content_label)
        self.setLayout(vbox)

    def select_batch(self):

        name, _ = QFileDialog.getOpenFileName(self, 'Open File', self.BATCH_DIR, '*.csv')
        
        if not name:
            return

        with open(name) as a_file:
            new_list = spreadsheet_to_plan_list(spreadsheet_file=a_file, file_name=name)
            self.content = new_list
            self._set_label()

    def _verify_plans(self, plan_list):
        for plan in plan_list:
            print(dir(plan))
    
    def _set_label(self):
        text = ''
        if self.content:
            for plan in self.content:
                text = text + str(plan) + '\n\n'
        self.content_label.setText(text)


    def get_value(self):
        return [str(c) for c in self.content]
    
    def set_value(self, value):
        self.content = value
        self._set_label()
    
    def clear(self):
        self.content = None
        self._set_label()
    