#contains widgets that can depict the status of certain
#pvs using pyepics and colors

from qtpy.QtWidgets import (
    QWidget,
    QPushButton,
    QHBoxLayout,
    QVBoxLayout,
    QGridLayout,
    QRadioButton,
    QGroupBox,
    QLabel,
)
import epics
from qtpy.QtCore import Qt, Signal, Slot, QTimer

#self._button_color_map['black'] = "QRadioButton::indicator{background-color : black}"

class QtSinglePV(QRadioButton):

    def __init__(self, parent, pvname, color_map):
        super().__init__(parent)
        self.pvname = pvname
        self.color_map = color_map
        self.setLayoutDirection(1)
        self.setCheckable(False)
        self.set_color('black')
        self.pv = epics.PV(self.pvname)
        self.pv.add_callback(self._set_status_led)
    
    def _get_color(self, value):
        for col, fnc in self.color_map.items():
            try:
                if fnc(value):
                    return col
            except TypeError as e:
                pass
        raise TypeError('No given Callback could handle the value of ' + self.pvname)

    def set_color(self, color):
        self.setStyleSheet("QRadioButton::indicator{background-color : " + color + "}")
    
    def _set_status_led(self, *args, value, **kwargs):
        color = self._get_color(value)
        self.set_color(color)

class QtPVStatusViewer2Columns(QWidget):
    def __init__(self, *args, name, left, right, legend, **kwargs):
        #expects two dicts where the key is the text to be shown and the value is (pvname, color_map)
        #color_map should be a list of tuples.
        #each tuple contains a lambda expression with a boolean, a color key word and the label for the legend
        #example: (lambda x: x<1, 'red', 'Red means smaller than 1)
        super().__init__(*args, **kwargs)
        self.legend_map = legend
        self.legend_cut = len(legend.keys()) / 2
        group_box = QGroupBox(name)
        vbox_00 = QVBoxLayout()

        status_box = QGroupBox()
        hbox_10 = QHBoxLayout()
        vbox_20 = QVBoxLayout()
        for key in left.keys():
            led = QtSinglePV(self, left[key][0], left[key][1])
            led.setText(key)
            vbox_20.addWidget(led)
        hbox_10.addLayout(vbox_20)
        hbox_10.addSpacing(10)
        vbox_21 = QVBoxLayout()
        for key in right.keys():
            led = QtSinglePV(self, right[key][0], right[key][1])
            led.setText(key)
            vbox_21.addWidget(led)
        hbox_10.addLayout(vbox_21)
        status_box.setLayout(hbox_10)
        
        legend_box = QGroupBox('Legend')
        hbox_11 = QHBoxLayout()
        vbox_23 = QVBoxLayout()
        vbox_24 = QVBoxLayout()
        for i, (col, txt) in enumerate(self.legend_map.items()):
            btn = QRadioButton()
            btn.setLayoutDirection(0)
            btn.setCheckable(False)
            btn.setStyleSheet("QRadioButton::indicator{background-color : " + col + "}")
            btn.setText(txt)
            if i < self.legend_cut:
                vbox_23.addWidget(btn)
            else:
                vbox_24.addWidget(btn)
        hbox_11.addLayout(vbox_23)
        hbox_11.addLayout(vbox_24)
        legend_box.setLayout(hbox_11)     

        vbox_00.addWidget(status_box)
        vbox_00.addWidget(legend_box)
        group_box.setLayout(vbox_00)
        top_box = QHBoxLayout()
        top_box.addWidget(group_box)
        self.setLayout(top_box)

class QtPhotonshutterCtrl(QWidget):
    signal_update_widget = Signal(object)
    def __init__(self, *args, model, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model

class QtElogSelection(QWidget):
    signal_update_widget = Signal(object)
    signal_run_engine_update = Signal(object)

    def __init__(self, *args, model, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        grp_box = QGroupBox('Selected Elog Entry')
        box = QGridLayout()
        self.caption_title = QLabel('Title: ')
        box.addWidget(self.caption_title, 0, 0)
        self.caption_name = QLabel('Name: ')
        box.addWidget(self.caption_name, 1, 0)
        self.caption_id = QLabel('ID: ')
        box.addWidget(self.caption_id, 2, 0)

        self.selected_title = QLabel('')
        box.addWidget(self.selected_title, 0, 1)
        self.selected_name = QLabel('')
        box.addWidget(self.selected_name, 1, 1)
        self.selected_id = QLabel('')
        box.addWidget(self.selected_id, 2, 1)

        self.caption_run_engine = QLabel('RunEngine: ')
        box.addWidget(self.caption_run_engine, 3, 0)

        self.run_engine_status = QLabel('')
        box.addWidget(self.run_engine_status, 3, 1)

        grp_box.setLayout(box)
        tbox = QVBoxLayout()
        tbox.addWidget(grp_box)
        self.setLayout(tbox)

        self.slot_update_selection()
        self.model.events.investigation_changed.connect(self.update_widgets)
        self.signal_update_widget.connect(self.slot_update_selection)

    def update_widgets(self, event):
        self.signal_update_widget.emit(event.status)

    def update_run_engine_status(self, event):
        self.signal_run_engine_update.emit()

    @Slot(object)
    def slot_update_selection(self, modus='default'):
        if modus == 'default':
            self.selected_title.setText('Nothing selected.')
            self.selected_name.setText('Nothing selected.')
            self.selected_id.setText('Nothing selected.')
        elif modus == 'selection':
            self.selected_title.setText(self.model.selected_title)
            self.selected_name.setText(self.model.selected_name)
            self.selected_id.setText(self.model.selected_id)