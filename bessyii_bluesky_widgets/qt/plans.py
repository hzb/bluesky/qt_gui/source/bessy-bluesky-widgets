plans = {
    'count': {
        'plan': {
            'detectors': {'type': 'detector_input', 'depends_on': ''},
            'num': {'type': 'numeric_input', 'depends_on': '', 'unit': ''},
            'delay': {'type': 'numeric_input', 'depends_on': 'detectors', 'unit': 'sec'},
        },
        'md': {
            'reason': {'type': 'text_input', 'depends_on': ''},
        },
        'sample': {
            'name': {'type': 'text_input', 'depends_on': ''},
            'description': {'type': 'text_input', 'depends_on': ''},
        },
    },

    'scan': {
        'plan': {
            'motor': {'type': 'motor_input', 'depends_on': ''},
            'start': {'type': 'numeric_input', 'depends_on': '', 'unit': ''},
            'stop': {'type': 'numeric_input', 'depends_on': '', 'unit': ''},
            'num': {'type': 'numeric_input', 'depends_on': '', 'unit': ''},
            'detectors': {'type': 'detector_input', 'depends_on': 'motor'},
        },
        'md': {
            'reason': {'type': 'text_input', 'depends_on': ''},
        },
        'sample': {
            'name': {'type': 'text_input', 'depends_on': ''},
            'description': {'type': 'text_input', 'depends_on': ''},
        },
    }
}