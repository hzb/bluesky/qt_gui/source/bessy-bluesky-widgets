from qtpy.QtWidgets import (
    QWidget,
    QGridLayout,
    QComboBox,
    QPushButton,
    QLabel,

)
from bluesky_widgets.models.plot_builders import Lines
from bluesky_widgets.models.plot_specs import Figure, Axes
#from ..plots.figure import QtFigures
from .new_plot_widget import QtFigures

from bessyii_bluesky_widgets.qt.detach_tab import (
    QtDetachableTabWidget,
)

def replace_inheritance():
    global QtFigures

    bases = QtDetachableTabWidget.mro().copy()
    bases.insert(0, QtFigures)

    class_dict = {}
    #make sure the override works corretly
    for base in reversed(bases):
        class_dict.update(base.__dict__)

    QtFigures = type('QtFigures', tuple(bases), class_dict)

replace_inheritance()

class QtAddCustomPlot(QWidget):
    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        layout = QGridLayout()
        self.setLayout(layout)

        self.x_selector = QComboBox(self)
        self.y_selector = QComboBox(self)

        new_button = QPushButton("New")
        self.add_button = QPushButton("Add")
        self.add_button.setEnabled(False)

        layout.addWidget(QLabel("x axis:"), 0, 0, 1, 1)
        layout.addWidget(QLabel("y axis:"), 1, 0, 1, 1)
        layout.addWidget(self.x_selector, 0, 1, 1, 2)
        layout.addWidget(self.y_selector, 1, 1, 1, 2)
        layout.addWidget(new_button, 0, 3, 1, 1)
        layout.addWidget(self.add_button, 1, 3, 1, 1)

        self.x_selector.setEditable(True)
        self.y_selector.setEditable(True)

        self.x_selector.setCurrentIndex(-1)
        self.y_selector.setCurrentIndex(-1)

        new_button.clicked.connect(self._on_new_button_clicked)
        self.add_button.clicked.connect(self._on_add_button_clicked)
        active_search_model = self.model.search
        active_search_model.events.active_run.connect(self._on_active_run_selected)
        # Keep this on Lines only for now
        #self.model.auto_plot_builders[0].figures.events.active_index.connect(self._on_active_figure_changed)

        if self.model.auto_plot_builders[0].figures.active_index is None:
            return
        self.model.auto_plot_builders[0].figures.events.active_index.connect(self._on_active_figure_changed)
        self.x_selector.currentTextChanged.connect(self._on_x_selector_text_changed)

    def _on_active_run_selected(self, event):
        self.x_selector.clear()
        self.y_selector.clear()
        for stream in self.model.search.active_run:
            if stream == "primary":
                self.x_selector.addItems(self.model.search.active_run[stream].to_dask().keys())
                self.y_selector.addItems(self.model.search.active_run[stream].to_dask().keys())
        
        self.x_selector.addItem("time")
        self.y_selector.addItem("time")

    def _on_new_button_clicked(self):
        axes = Axes(
                x_label=self.x_selector.currentText(),
                y_label=self.y_selector.currentText()
            )
        figure = Figure((axes,), title=self.y_selector.currentText())
     
        line = Lines(x=self.x_selector.currentText(), ys=[self.y_selector.currentText()],axes=axes, max_runs=3)

        if self.model.search.active_run:
            line.add_run(self.model.search.active_run)

        # Keep this on Lines only for now
        self.model.auto_plot_builders[0].plot_builders.append(line)
        self.model.auto_plot_builders[0].figures.append(line.figure)

    def _on_add_button_clicked(self):
        # Keep this on Lines only for now
        if self.model.auto_plot_builders[0].figures.active_index is None:
            return
        else:
            self.model.auto_plot_builders[0].figures.events.active_index.connect(self._on_active_figure_changed)
        active_index = self.model.auto_plot_builders[0].figures.active_index
        active_uuid = list(self.model._figures_to_lines.keys())[active_index]
        for line in self.model._figures_to_lines[active_uuid]:
            line.ys.append(self.y_selector.currentText())

    def _on_active_figure_changed(self, event):
        # Keep this on Lines only for now
        if self.model.auto_plot_builders[0].figures.active_index is None:
            return      
        
        
        active_index = self.model.auto_plot_builders[0].figures.active_index
        active_figure = self.model.auto_plot_builders[0].figures[active_index]
        self.x_selector.setCurrentText(active_figure.axes[0].x_label)
        self.add_button.setEnabled(True)

    def _on_x_selector_text_changed(self, text):
        # Keep this on Lines only for now
        if self.model.auto_plot_builders[0].figures.active_index is None:
            return
        active_index = self.model.auto_plot_builders[0].figures.active_index
        active_figure = self.model.auto_plot_builders[0].figures[active_index]
        if text != active_figure.axes[0].x_label:
            self.add_button.setEnabled(False)
        else:
            self.add_button.setEnabled(True)