from qtpy.QtWidgets import QWidget, QLabel, QApplication, QVBoxLayout, QMainWindow
from qtpy.QtCore import QThread, Slot, Signal, QObject, Qt, QTimer
from qtpy.QtGui import QFontInfo, QFont

import queue
import time

from bluesky_widgets.qt.run_engine_client import QtReConsoleMonitor

import sys

from bluesky.callbacks.zmq import RemoteDispatcher
from bluesky_widgets.qt.zmq_dispatcher import RemoteDispatcher as QtRemoteDispatcher
from bluesky.callbacks.best_effort import BestEffortCallback
from .lessEffortCallback import LessEffortCallback

from ..models.document_model import DocumentModel



def zmq_table(out=print,continue_polling=None):
    bec = LessEffortCallback(out=out)
    # bec = BestEffortCallback()

    zmq_dispatcher = RemoteDispatcher('localhost:5578')

    zmq_dispatcher.subscribe(bec)
    zmq_dispatcher.start()


def qt_zmq_table(out=print):
    bec = LessEffortCallback(out=out)
    # bec = BestEffortCallback()

    zmq_dispatcher = QtRemoteDispatcher('localhost:5578')

    zmq_dispatcher.subscribe(bec)

    return zmq_dispatcher

class LiveTableModel(QWidget):
    def __init__(self, parent=None, model=None):
        super().__init__(parent)
        self.msg_queue = queue.Queue()
        bec = LessEffortCallback(out=self.newMsg)
        self.model = model
        self.model.add_callback(bec)
        #self.zmq_dispatcher = qt_zmq_table(
        #self.newMsg
        #)
        #self.zmq_dispatcher.setParent(self)
        #self.zmq_dispatcher.start()

        #self.destroyed.connect(lambda: self.stop_console_output_monitoring)

    def newMsg(self, msg):
        self.msg_queue.put(msg)
        # print(msg)

    def start_console_output_monitoring(self):
        print("Start Console Output Monitoring")
        self._stop_console_monitor = False

    def stop_console_output_monitoring(self):
        print("Stop Console Monitoring")
        #self.zmq_dispatcher.stop()
        self.model.close()
        self._stop_console_monitor = True

    def continue_polling(self):
        return not self._stop_console_monitor

    # def console_monitoring_thread(self, *, callback):
    def console_monitoring_thread(self):
        # print("Monitoring")
        for n in range(5):
            try:
                msg = self.msg_queue.get(timeout=0.2)
                msg = msg.rstrip("\n") + "\n"
                msgtime = time.time()
                return msgtime, msg

            except queue.Empty:
                pass
            except Exception as ex:
                print(f"Exception occurred: {ex}")

            if self._stop_console_monitor:
                print("Stop monitoring!")
        return None, None


class QtZMQTableTab(QWidget):
    name = "Live Table"

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self.zmqTable = LiveTableModel(self, model=self.model.documents)
        self.zmqMonitor = QtReConsoleMonitor(self.zmqTable, self)

        # Printing the font and font family used by self.zmqMonitor
        font = self.zmqMonitor._text_edit.font()
        font.setFamily("Monospace")
        font.setStyleHint(QFont.Monospace)
        self.zmqMonitor._text_edit.setFont(font)

        vbox = QVBoxLayout()
        vbox.addWidget(QLabel("ZMQ Table Monitor"))
        vbox.addWidget(self.zmqMonitor)
        self.setLayout(vbox)

        font = self.zmqMonitor._text_edit.font()
        actual_font = QFontInfo(font)
        print(f"Font used: {actual_font.family()}, Font Desired: {font.family()}")