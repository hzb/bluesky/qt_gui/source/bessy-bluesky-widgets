#model.events.running_item_changed

import gc
import time
import os

from qtpy.QtWidgets import (
    QSizePolicy,
    QTabWidget,
    QWidget,
    QVBoxLayout,
) 
from qtpy.QtCore import Qt, Signal, Slot, QTimer, QPoint, QObject
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar,
)
import matplotlib.figure

from bluesky_widgets.models.plot_specs import Figure, FigureList
from bluesky_widgets._matplotlib_axes import MatplotlibAxes
from bluesky_widgets.utils.event import Event
from bluesky_widgets.utils.dict_view import DictView

from bluesky_widgets.qt.threading import create_worker

import matplotlib

matplotlib.use("Qt5Agg")  # must set before importing matplotlib.pyplot
import matplotlib.pyplot  # noqa
import matplotlib.animation as anim


def _initialize_matplotlib():
    "Set backend to Qt5Agg and import pyplot."
    import matplotlib

    matplotlib.use("Qt5Agg")  # must set before importing matplotlib.pyplot
    import matplotlib.pyplot  # noqa
    import matplotlib.animation as anim


class QtFigures(QTabWidget):
    """
    A Jupyter (ipywidgets) view for a FigureList model.
    """

    __callback_event = Signal(object, Event)
    __running_item_changed = Signal(object)

    def __init__(self, model, parent=None):
        _initialize_matplotlib()
        super().__init__(parent)
        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self._on_close_tab_requested)
        self.resize(self.sizeHint())

        #self.figure_list = model.live_auto_plot_builders[0].figures
        self.model = model
        # Map Figure UUID to widget with QtFigureTab
        self._figures = {}
        self._is_live_plotting = False

        #for figure_spec in self.figure_list:
        #    self._add_figure(figure_spec)
        self._threadsafe_connect(self.model.events.running_item_changed, self.test_events)
        self.__running_item_changed.connect(self.slot_running_item_changed)
        #self._threadsafe_connect(self.figure_list.events.added, self._on_figure_added)
        #self._threadsafe_connect(self.figure_list.events.removed, self._on_figure_removed)

        # This setup for self._threadsafe_connect.

        def handle_callback(callback, event):
            callback(event)

        self.__callback_event.connect(handle_callback)

        self.currentChanged.connect(self._on_tab_changed)

    def test_events(self, event):
        if not event.running_item == {}:
            self.__running_item_changed.emit(event.running_item)

    @Slot(object)
    def slot_running_item_changed(self, item):
        self._add_figure(item)

    def sizeHint(self):
        size_hint = super().sizeHint()
        size_hint.setWidth(700)
        size_hint.setHeight(500)
        return size_hint

    @property
    def figures(self):
        "Read-only access to the mapping Figure UUID -> QtFigure"
        return DictView(self._figures)

    def _threadsafe_connect(self, emitter, callback):
        """
        A threadsafe method for connecting to models.

        For example, instead of

        >>> model.events.addded.connect(callback)

        use

        >>> self._threadsafe_connect(model.events.added, callback)
        """
        emitter.connect(lambda event: self.__callback_event.emit(callback, event))

    def _on_close_tab_requested(self, index):
        # When closing is initiated from the view, remove the associated
        # model.
        widget = self.widget(index)
        #self.figure_list.remove(widget.figure_list)

    def _on_figure_added(self, event):
        figure_spec = event.item
        self._add_figure(figure_spec)

    def _add_figure(self, item):
        "Add a new tab with a matplotlib Figure."
        tab = QtFigure(parent=self)
        self.addTab(tab, 'Test')
        self._figures['1234'] = tab

    def _on_figure_removed(self, event):
        "Remove the associated tab and close its canvas."
        figure_spec = event.item
        widget = self._figures[figure_spec.uuid]
        index = self.indexOf(widget)
        self.removeTab(index)
        widget.close_figure()
        del widget
        gc.collect()
        del self._figures[figure_spec.uuid]

    def _on_short_title_changed(self, event):
        "This sets the tab title."
        figure_spec = event.figure_spec
        widget = self._figures[figure_spec.uuid]
        index = self.indexOf(widget)
        # Fall back to title if short_title is being unset.
        if event.value is None:
            self.setTabText(index, figure_spec.title)
        else:
            self.setTabText(index, event.value)

    def _on_title_changed(self, event):
        "This sets the tab title only if short_title is None."
        figure_spec = event.figure_spec
        if figure_spec.short_title is None:
            widget = self._figures[figure_spec.uuid]
            index = self.indexOf(widget)
            self.setTabText(index, event.value)

    def _on_tab_changed(self, index):
        "Update the active_index in the FigureList."
        #self.figure_list.active_index = index


class QtFigure(QWidget):
    """
    A Qt view for a Figure model. This always contains one Figure.
    """

    def __init__(self, parent=None):
        #_initialize_matplotlib()
        super().__init__(parent)
        # TODO Let Figure give different options to subplots here,
        # but verify that number of axes created matches the number of axes
        # specified.

        #self.canvas = FigureCanvas(self.figure)
        self.canvas = MyFigureCanvas()
        self.canvas.setMinimumWidth(640)
        self.canvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.canvas.updateGeometry()
        self.canvas.setParent(self)
        toolbar = NavigationToolbar(self.canvas, parent=self)

        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        layout.addWidget(toolbar)
        self.setLayout(layout)
        self.resize(self.sizeHint())

        # The Figure model does not currently allow axes to be added or
        # removed, so we do not need to handle changes in model.axes.

    def sizeHint(self):
        size_hint = super().sizeHint()
        size_hint.setWidth(700)
        size_hint.setHeight(500)
        return size_hint

    @property
    def axes(self):
        "Read-only access to the mapping Axes UUID -> MatplotlibAxes"
        return DictView(self._axes)

    def _on_title_changed(self, event):
        self.figure.suptitle(event.value)
        self._redraw()

    def _redraw(self):
        "Redraw the canvas."
        # Schedule matplotlib to redraw the canvas at the next opportunity, in
        # a threadsafe fashion.
        self.figure.canvas.draw_idle()

    def close_figure(self):
        self.figure.canvas.close()

class MyFigureCanvas(FigureCanvas):
    '''
    This is the FigureCanvas in which the live plot is drawn.

    '''
    def __init__(self):
        '''
        :param x_len:       The nr of data points shown in one plot.
        :param y_range:     Range on y-axis.
        :param interval:    Get a new datapoint every .. milliseconds.

        '''
        API_KEY = os.environ['QSERVER_API_KEY']
        from tiled.client import from_uri
        self.client = from_uri("http://127.0.0.1:8000", api_key=API_KEY)

        FigureCanvas.__init__(self, matplotlib.figure.Figure())

        self.x = []
        self.y = []

        # Store a figure and ax
        self._ax_  = self.figure.subplots()
        self._line_, = self._ax_.plot(self.x, self.y)

        # Call superclass constructors
        self.ani = anim.FuncAnimation(self.figure, self._update_canvas_, interval=500)

        #print(self.model.events.source)

    def _update_canvas_(self, i):
        '''
        This function gets called regularly by the timer.
        '''
        try:
            self.x.append(self.client.items().last()[1].get('primary').data.get('time').read([len(self.x), len(self.x)+1]))
            self.y.append(self.client.items().last()[1].get('primary').data.get('kth1').read([len(self.y), len(self.y)+1]))
        except IndexError as e:
            pass
        
        self._ax_.clear()
        self._ax_.plot(self.x, self.y)

    def stop(self):
        self.ani.event_source.stop()

    def restart(self):
        self.ani.event_source.start()
