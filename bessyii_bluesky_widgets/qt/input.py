#gneral input classes

from qtpy.QtWidgets import (
    QWidget,
    QLabel,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QFileDialog,
    QLineEdit,
    QGroupBox,
    QGridLayout,
    QDialog,
    QListWidget,
    QFrame,
)

from bluesky_widgets.qt.threading import *
import Levenshtein as lev

from qtpy.QtCore import Qt, Signal, Slot, QTimer, QPoint

class QtPlanButton(QWidget):
    signal_update_widget = Signal(object)
    def __init__(self, *args, model, name, plan, **kwargs):
        super().__init__(*args, **kwargs)
        self.button = QPushButton(name)
        self.button.setEnabled(False)
        self.model = model
        #plan is the name (String) of a plan that is present in the iPython profile of the Q-Server
        self.plan = plan
        self.button.clicked.connect(self._on_clicked)
        self.label = QLabel("")
        self.label.setStyleSheet("color:red;font:5pt;")
        #self.label.setVisible(False)
        vbox = QVBoxLayout()
        vbox.setSpacing(0)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(self.button)
        vbox.addWidget(self.label)
        self.setLayout(vbox)
        self.model.events.status_changed.connect(self.on_update)
        self.signal_update_widget.connect(self.slot_update_widgets)
    
    def on_update(self, event):
        is_connected = event.is_connected
        self.signal_update_widget.emit(is_connected)
    
    @Slot(object)
    def slot_update_widgets(self, is_connected):
        if is_connected != None:
            plan_found = self.plan in self.model.get_allowed_plan_names()
            if is_connected and not plan_found:
                self.label.setText(self.plan + ' is not available.')
            else:
                self.label.setText("")
            self.button.setEnabled(is_connected and plan_found)
        else:
            self.button.setEnabled(False)
            self.label.setText("")

    def _on_clicked(self):
        item = {'item_type': 'plan',
                'name': self.plan,
                'args': [],
                'kwargs': {},
                'meta': {}}
        self.model.queue_item_add(item=item)

class QtExportButton(QWidget):
    signal_update_widget = Signal(object)
    def __init__(self, *args, name, model, callback, **kwargs):
        super().__init__(*args, **kwargs)
        self.button = QPushButton(name)
        self.button.setEnabled(False)
        self.model = model
        self.callback = callback

        self.button.clicked.connect(self._on_clicked)
        vbox = QVBoxLayout()
        vbox.addWidget(self.button)
        self.setLayout(vbox)
        self.model.events.active_run.connect(self.on_update)
        self.signal_update_widget.connect(self.slot_update_widgets)
    
    def on_update(self, event):
        self.signal_update_widget.emit(event is not None)
    
    @Slot(object)
    def slot_update_widgets(self, is_selected):
        if is_selected != None:
            self.button.setEnabled(is_selected)

    def _on_clicked(self):
        folderpath = QFileDialog.getExistingDirectory(self, 'Select Folder')
        if folderpath == '':
            return

        catalog = self.model.selection_as_catalog
        for key in catalog.keys():
            self.callback(folderpath, catalog[key])
        self.button.setEnabled(False)
 
class QtAuthButton(QWidget):
    signal_update_widget = Signal(object)

    def __init__(self, *args, model, sys_name, **kwargs):
        super().__init__(*args, **kwargs)
        self.system = sys_name
        vbox = QVBoxLayout()
        #label = QLabel('Elog:')
        #vbox.addWidget(label)
        self.button = QPushButton(self.system + ' not connected.')
        self.button.setStyleSheet("color: black;")
        self.button.clicked.connect(self.pressed)
        vbox.addWidget(self.button)
        self.setLayout(vbox)
        self.auth_box = QtAuthBox(self.parent(), caption=self.system + ' BoX', model=model)
        self.model = model

        self.model.events.status_changed.connect(self.status_changed)
        self.signal_update_widget.connect(self.slot_update_widgets)
        self.setFixedSize(self.layout().sizeHint())

    def status_changed(self, event):
        self.signal_update_widget.emit(event)
    
    @Slot(object)
    def slot_update_widgets(self, event):
        if event.status == 'connecting':
            self.button.setText('connecting..')
            self.button.setStyleSheet("color: yellow;")
        else:
            if self.model.connected:
                self.button.setText(self.system + '-User: ' + self.model.username)
                self.button.setStyleSheet("color: green;")
            else:
                self.button.setText(self.system + ' not connected.')
                self.button.setStyleSheet("color: black;")

    def pressed(self):
        if self.auth_box.isVisible():
            self.pushed = False
            self.auth_box.stop()
        else:
            self.pushed = True
            self.auth_box.start(self.mapToParent(self.rect().bottomLeft()), self.mapToParent(self.rect().topRight()))



class QtAuthBox(QFrame):
    signal_update_widget = Signal(object)
    signal_update_inv = Signal(object)

    def __init__(self, *args, caption, model, **kwargs):
        super().__init__(*args, **kwargs)        

        self.setFrameShape(QFrame.StyledPanel)
        self.setLineWidth(6)

        self.investigation_number = None
        self.user = None
        self.model = model

        self.grp_box = QGroupBox(caption)
        vbox_00 = QVBoxLayout()

        self.username = QLineEdit()
        self.username.textChanged.connect(self.disable_button)
        self.username.returnPressed.connect(self.return_pressed)
        vbox_00.addWidget(self.username)
        self.password = QLineEdit()
        self.password.setEchoMode(QLineEdit.Password)
        self.password.returnPressed.connect(self.return_pressed)
        self.password.textChanged.connect(self.disable_button)
        vbox_00.addWidget(self.password)

        hbox_10 = QHBoxLayout()
        self.login = QPushButton('Log In')
        self.login.setMinimumSize(80, 25)
        self.login.clicked.connect(self.login_clicked)
        self.login.setEnabled(False)
        hbox_10.addWidget(self.login)

        self.logout = QPushButton('Log Out')
        self.logout.setMinimumSize(80, 25)
        self.logout.clicked.connect(self.logout_clicked)
        self.logout.setEnabled(False)
        hbox_10.addWidget(self.logout)
        vbox_00.addLayout(hbox_10)

        self.change_inv = QPushButton('Change Investigation')
        self.change_inv.setMinimumSize(180, 25)
        self.change_inv.clicked.connect(self.change_clicked)
        self.change_inv.setEnabled(False)
        vbox_00.addWidget(self.change_inv)

        vbox = QVBoxLayout()
        self.grp_box.setLayout(vbox_00)
        vbox.addWidget(self.grp_box)

        self.inv_title = QLabel('')
        self.inv_title.setStyleSheet("font:10pt")
        self.inv_title.setWordWrap(True)
        vbox.addWidget(self.inv_title)

        self.model.events.status_changed.connect(self.status_update)
        self.model.events.investigation_changed.connect(self.inv_update)
        self.signal_update_widget.connect(self.slot_update_widgets)
        self.signal_update_inv.connect(self.slot_update_inv)

        self.setLayout(vbox)
        self.setAutoFillBackground(True)
        self.setFixedSize(self.layout().sizeHint())
        self.hide()

    def start(self, btn_bl, btn_tr):
        self._calc_position(btn_bl, btn_tr)
        self.raise_()
        self.show()
    
    def _calc_position(self, btn_bl, btn_tr):
        x = btn_bl.x()
        if x > self.parent().width() / 2:
            x = btn_tr.x() - self.width()
        
        y = btn_bl.y()
        if y > self.parent().height() / 2:
            y = btn_tr.y() - self.height()
        self.setGeometry(x, y, self.width(), self.height())

    def stop(self):
        self.hide()

    def status_update(self, event):
        self.signal_update_widget.emit(event)
    
    def inv_update(self, event):
        self.signal_update_inv.emit(event)
    
    @Slot(object)
    def slot_update_widgets(self, event):
        self.change_status(event.status)
    
    @Slot(object)
    def slot_update_inv(self, event):
        self.inv_title.setText(self.model.selected_title)
        self.setFixedSize(self.layout().sizeHint())
    
    def login_clicked(self):
        worker = create_worker(self.model.requestInvestigationNames, self.username.text(), self.password.text())
        worker.start()
    
    def change_clicked(self):
        self.show_investigations(self.model.investigations)

    def disable_widgets(self, disable=True):
        #Make the boolean behavior easier to understand from outside disable(True) will disable and the other way around
        enable = not disable
        self.login.setEnabled(enable)
        self.username.setEnabled(enable)
        self.password.setEnabled(enable)
        self.logout.setEnabled(enable)
        self.change_inv.setEnabled(enable)

    def logout_clicked(self):
        self.model.logout()
        self.stop()

    def return_pressed(self):
        if len(self.username.text()) > 0 and len(self.password.text()) > 0:
            self.login_clicked()
    
    def disable_button(self):
        activate = len(self.username.text()) > 0 and len(self.password.text()) > 0
        self.login.setEnabled(activate)

    def change_status(self, status):
        if status == 'default':
            self.password.setText('')
            self.disable_widgets(False)
            self.logout.setEnabled(False)
            self.change_inv.setEnabled(False)
        elif status == 'failed':
            self.disable_widgets(False)
            self.logout.setEnabled(False)
            self.change_inv.setEnabled(False)
        elif status == 'connecting':
            self.disable_widgets(True)
        elif status == 'connected':
            self.disable_widgets(True)
            self.logout.setEnabled(True)
            self.show_investigations(self.model.investigations)
            self.change_inv.setEnabled(True)
    
    def show_investigations(self, inv_dict):
        dialog = QtInvestigationList(inv_dict = inv_dict)
        selection = dialog.selectInv()
        self.model.select_investigation(selection)
        self.stop()

class QtInvestigationList(QDialog):
    def __init__(self, *args, inv_dict, **kwargs):
        super().__init__(*args, **kwargs)
        self.setWindowTitle('Select an Investigation')
        self.setWindowModality(Qt.ApplicationModal)
        self.selected_inv = None
        self.confirmed_inv = None
        self.inv_dict = inv_dict
        self.shown_dict = inv_dict

        box = QVBoxLayout()
        self.list_widget = QListWidget()
        self.list_widget.addItems(inv_dict.keys())
        self.list_widget.setMinimumHeight(500)
        self.list_widget.setMinimumWidth(500)
        self.list_widget.setStyleSheet("font: 12pt")
        self.list_widget.itemClicked.connect(self.on_select)
        

        self.search_input = QLineEdit()
        self.search_input.setPlaceholderText('Search..')
        self.search_input.textChanged.connect(self.on_search)
        

        self.selected_label = QLabel('Nothing Selected')
        self.selected_label.setStyleSheet("font:10pt")
        self.selected_label.setWordWrap(True)

        self.confirm_btn = QPushButton('Select')
        self.confirm_btn.clicked.connect(self.confirm_clicked)
        self.confirm_btn.setStyleSheet('font: 12pt')
        self.confirm_btn.setEnabled(False)

        box.addWidget(self.search_input)
        box.addWidget(self.list_widget)
        box.addWidget(self.confirm_btn)
        box.addWidget(self.selected_label)

        self.setLayout(box)
    
    def selectInv(self):
        self.exec_()
        if self.confirmed_inv is None:
            return None
        else:
            return {
                'title': self.confirmed_inv,
                'name': self.inv_dict[self.confirmed_inv]['name'],
                'id': self.inv_dict[self.confirmed_inv]['id']
            }
    
    def on_search(self):
        part_title = self.search_input.text()
        if len(part_title) > 0:
            possible_keys = []
            for key in self.inv_dict.keys():
                title = key
                Distance = lev.distance(title.lower(),part_title.lower())
                Ratio = lev.ratio(title.lower(),part_title.lower())
                if part_title in title or part_title.lower() in title.lower() or Ratio > 0.8 or Distance < 10:
                    possible_keys.append(key)
            
            self.list_widget.clear()
            self.list_widget.addItems(possible_keys)
        else:
            self.list_widget.clear()
            self.list_widget.addItems(self.inv_dict.keys())

    def on_select(self, item):
        self.confirm_btn.setEnabled(True)
        self.selected_inv = item.text()
        self.selected_label.setText(self.selected_inv)
        self.setFixedSize(self.layout().sizeHint())

    def confirm_clicked(self):
        self.confirmed_inv = self.selected_inv
        self.close()
