from qtpy.QtWidgets import (
    QWidget,
    QGroupBox,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QTableWidget,
    QTableWidgetItem,
    QTableView,
    QHeaderView,
    QAbstractItemView,
    QTextEdit,
    QTabWidget,
    QRadioButton,
    QButtonGroup,
    QComboBox,
    QLineEdit,
    QCheckBox,
    QDialog,
    QFileDialog,
    QDialogButtonBox,
)
from bluesky_queueserver.manager.conversions import spreadsheet_to_plan_list
from bluesky_queueserver.manager.tests.plan_lists import create_excel_file_from_plan_list

from bluesky_widgets.qt.run_engine_client import (
    QueueTableWidget,
    PushButtonMinimumWidth,
)
from qtpy.QtCore import Qt, Signal, Slot, QTimer, QSize
from qtpy.QtGui import QFontMetrics, QPalette, QBrush, QColor, QIntValidator, QIcon
import copy
import os
import time


class QtQueueControl(QWidget):

    signal_update_widgets = Signal(object)
    signal_update_selection = Signal(object)
    signal_plan_queue_changed = Signal(object, object)
    signal_running_item_changed = Signal(object)

    def __init__(self, model, parent=None):
        super().__init__(parent)
        self.model = model
        self._monitor_mode = False

        # Set True to block processing of table selection change events
        self._block_table_selection_processing = False

        self._registered_item_editors = []

        # Local copy of the plan queue items for operations performed locally
        #   in the Qt Widget code without calling the model. Using local copy that
        #   precisely matches the contents displayed in the table is more reliable
        #   for local operations (e.g. calling editor when double-clicking the row).
        self._plan_queue_items = []

        self._stop_flag = False
        self._skip_flag = False
        self._was_skipped = False

        self._table_column_labels = (
            "",
            "Name",
            "Parameters",
            "USER",
            "GROUP",
        )
        self._table = QueueTableWidget()
        self._table.setColumnCount(len(self._table_column_labels))
        self._table.setHorizontalHeaderLabels(self._table_column_labels)
        self._table.horizontalHeader().setSectionsMovable(True)

        self._table.setVerticalScrollMode(QAbstractItemView.ScrollPerItem)
        self._table.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)

        self._table.setSelectionBehavior(QTableView.SelectRows)
        self._table.setSelectionMode(QTableWidget.ContiguousSelection)

        self._table.setDragEnabled(False)
        self._table.setAcceptDrops(False)
        self._table.setDropIndicatorShown(True)
        self._table.setShowGrid(True)

        # Prevents horizontal autoscrolling when clicking on an item (column) that
        # doesn't fit horizontally the displayed view of the table (annoying behavior)
        self._table.setAutoScroll(False)

        self._table.setAlternatingRowColors(True)

        self._table.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        self._table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self._table.horizontalHeader().setStretchLastSection(True)
        self._table.horizontalHeader().setMinimumSectionSize(5)

        self._table_scrolled_to_bottom = False

        # The following parameters are used only to control widget state (e.g. activate/deactivate
        #   buttons), not to perform real operations.
        self._n_table_items = 0  # The number of items in the table
        self._selected_items_pos = []  # Selected items (list of table rows)

        self._flag_queue_running = False
        self._flag_snapshot_pause = False
        self._flag_last_item_stopped = False
        self._queue_snapshot = []
        self.running_item = None

        self.debug = 0
        self._manager_state = None
        self._running_uid = None
        self._id_to_delete = None
        self._queue_len = 0

        
        ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
        self.BATCH_DIR = os.environ.get('BATCH_SAVE_DIR', os.path.join(ROOT_DIR, 'batches/'))
        self._start_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/start_en.png'))
        self._pause_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/pause_en.png'))
        self._stop_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/stop_en.png'))
        self._loop_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/loop_en.png'))
        self._clear_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/clear.png'))
        self._edit_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/edit.png'))
        self._delete_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/delete.png'))
        self._create_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/create.png'))
        self._clone_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/clone.png'))
        self._load_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/folder.png'))
        self._skip_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/skip.png'))
        self._reload_icon = QIcon(os.path.join(ROOT_DIR, 'Icons/reload.png'))
        self._start_btn = QPushButton()
        self._start_btn.setIcon(self._start_icon)
        self._start_btn.setToolTip('Start Queue')
        #self._start_btn.setIconSize(QSize(25,25))
        self._stop_btn = QPushButton()
        self._stop_btn.setIcon(self._stop_icon)
        self._stop_btn.setToolTip('Stop Queue')
        self._skip_btn = QPushButton()
        self._skip_btn.setIcon(self._skip_icon)
        self._skip_btn.setToolTip('Skip to next Plan')
        self._loop_btn = QPushButton()
        self._loop_btn.setIcon(self._loop_icon)
        self._loop_btn.setToolTip('Loop')
        self._loop_btn.setCheckable(True)
        self._clear_btn = QPushButton()
        self._clear_btn.setIcon(self._clear_icon)
        self._clear_btn.setToolTip('Clear Queue')
        self._del_btn = QPushButton()
        self._del_btn.setToolTip('Delete Selection')
        self._del_btn.setIcon(self._delete_icon)
        self._edit_btn = QPushButton()
        self._edit_btn.setToolTip('Edit Selected Plan')
        self._edit_btn.setIcon(self._edit_icon)
        self._clone_btn = QPushButton()
        self._clone_btn.setToolTip('Clone selected Plans')
        self._clone_btn.setIcon(self._clone_icon)

        self._batch_btn = QPushButton()
        self._batch_btn.setToolTip('Create Batch')
        self._batch_btn.setIcon(self._create_icon)
        self._load_btn = QPushButton()
        self._load_btn.setToolTip('Load Batch to Queue')
        self._load_btn.setIcon(self._load_icon)

        self._clear_btn.clicked.connect(self._pb_clear_queue_clicked)
        self._loop_btn.clicked.connect(self._pb_loop_on_clicked)
        self._start_btn.clicked.connect(self._pb_start_clicked)
        self._stop_btn.clicked.connect(self._pb_stop_clicked)
        self._skip_btn.clicked.connect(self._pb_skip_clicked)
        self._del_btn.clicked.connect(self._pb_delete_plan_clicked)
        self._edit_btn.clicked.connect(self._pb_edit_clicked)
        self._batch_btn.clicked.connect(self._pb_batch_clicked)
        self._clone_btn.clicked.connect(self._pb_clone_clicked)
        self._load_btn.clicked.connect(self._pb_load_clicked)

        self._group_box = QGroupBox("Plan Queue")
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("QUEUE"))
        hbox.addWidget(self._start_btn)
        hbox.addWidget(self._stop_btn)
        hbox.addWidget(self._skip_btn)
        hbox.addWidget(self._loop_btn)
        hbox.addStretch(1)
        hbox.addWidget(self._clone_btn)
        hbox.addWidget(self._edit_btn)
        hbox.addWidget(self._del_btn)
        hbox.addWidget(self._batch_btn)
        hbox.addWidget(self._load_btn)
        hbox.addStretch(2)
        hbox.addWidget(self._clear_btn)
        vbox.addLayout(hbox)
        vbox.addWidget(self._table)
        self.setLayout(vbox)

        self.model.events.status_changed.connect(self.on_update_widgets)
        self.signal_update_widgets.connect(self.slot_update_widgets)

        self.model.events.running_item_changed.connect(self.on_running_item_changed)
        self.signal_running_item_changed.connect(self.slot_running_item_changed)

        self.model.events.plan_queue_changed.connect(self.on_plan_queue_changed)
        self.signal_plan_queue_changed.connect(self.slot_plan_queue_changed)

        self.model.events.queue_item_selection_changed.connect(self.on_queue_item_selection_changed)
        self.signal_update_selection.connect(self.slot_change_selection)

        self._table.signal_drop_event.connect(self.on_table_drop_event)
        self._table.signal_scroll.connect(self.on_table_scroll_event)
        
        self._table.itemSelectionChanged.connect(self.on_item_selection_changed)
        self._table.verticalScrollBar().valueChanged.connect(self.on_vertical_scrollbar_value_changed)
        self._table.verticalScrollBar().rangeChanged.connect(self.on_vertical_scrollbar_range_changed)
        self._table.cellDoubleClicked.connect(self._on_table_cell_double_clicked)
        self._update_button_states()

    def on_running_item_changed(self, event):
        self.signal_running_item_changed.emit(event.running_item)

    @Slot(object)
    def slot_running_item_changed(self, running_item):
        if not running_item:
            self._flag_queue_running = False
        else:
            self._flag_queue_running = True
            self.running_item = copy.deepcopy(running_item)
            
    @property
    def monitor_mode(self):
        return self._monitor_mode

    @monitor_mode.setter
    def monitor_mode(self, monitor):
        self._monitor_mode = bool(monitor)
        self._update_widgets()

        if monitor:
            self._table.cellDoubleClicked.disconnect(self._on_table_cell_double_clicked)
        else:
            self._table.cellDoubleClicked.connect(self._on_table_cell_double_clicked)

    @property
    def registered_item_editors(self):
        """
        Returns reference to the list of registered plan editors. The reference is not editable,
        but the items can be added or removed from the list using ``append``, ``pop`` and ``clear``
        methods.

        Editors may be added to the list of registered plan editors by inserting/appending reference
        to a callable. The must accepts dictionary of item parameters as an argument and return
        boolean value ``True`` if the editor accepts the item. When user double-clicks the table row,
        the editors from the list are called one by one until the plan is accepted. The first editor
        that accepts the plan must be activated and allow users to change plan parameters. Typically
        the editors should be registered in the order starting from custom editors designed for
        editing specific plans proceeding to generic editors that will accept any plan that was
        rejected by custom editors.

        Returns
        -------
        list(callable)
            List of references to registered editors. List is empty if no editors are registered.
        """
        return self._registered_item_editors

    def on_update_widgets(self, event):
        # None should be converted to False:
        self.signal_update_widgets.emit(event)

    def _update_widgets(self, is_connected=None):
        if is_connected is None:
            is_connected = bool(self.model.re_manager_connected)

        # Disable drops if there is no connection to RE Manager
        self._table.setDragEnabled(is_connected and not self._monitor_mode)
        self._table.setAcceptDrops(is_connected and not self._monitor_mode)

        self._update_button_states()

    @Slot(object)
    def slot_update_widgets(self, event):
        manager_state = self.model.re_manager_status.get("manager_state", None)
        try:
            running_uid = event.status['running_item_uid']
            queue_empty = event.status['items_in_queue'] <= 0
        except KeyError as e:
            running_uid = None
            queue_empty = None
        except AttributeError as e:
            running_uid = None
            queue_empty = None

        if queue_empty and running_uid and self._stop_flag:
            self._flag_last_item_stopped = True

        if not manager_state == self._manager_state and not self._skip_flag:
            if manager_state == 'idle':
                if self._manager_state == 'executing_queue' or self._manager_state == 'paused':
                    self._manager_state = manager_state
                    queue_empty = not queue_empty is None and queue_empty
                    if self._queue_snapshot and (not queue_empty or self._flag_last_item_stopped):
                        self._flag_snapshot_pause = True
                        self.soft_reset_snapshot()
                    elif self._queue_snapshot and queue_empty:
                        self._flag_snapshot_pause = True
                    else:
                        self.reset_snapshot()
            elif manager_state == 'paused':
                if self._manager_state == 'executing_queue':
                    self._manager_state = manager_state
                    self.soft_reset_snapshot()
                else:
                    self._manager_state = manager_state
            elif manager_state == 'executing_queue':
                self._manager_state = manager_state
                self._running_uid = running_uid
                self._queue_len = len(self.model._plan_queue_items)
                self._table.clearSelection()
                self._table.setSelectionMode(0)
                self._table.setEnabled(False)
                if self._id_to_delete:
                    self.model.selected_queue_item_uids = [self._id_to_delete]
                    self._pb_delete_plan_clicked()
                    self._id_to_delete = None
                item_queue = copy.deepcopy(self._plan_queue_items)
                item_queue.insert(0, self.running_item)
                self.signal_plan_queue_changed.emit(item_queue, [])
        
        elif self._skip_flag and manager_state == 'paused':
            try:
                self.model.re_stop()
            except Exception as ex:
                print(f"Exception: {ex}")
        
        elif self._skip_flag and manager_state == 'idle':
            if len(self.model._plan_queue_items) > self._queue_len:
                self.model.selected_queue_item_uids = [self.model._plan_queue_items[0]['item_uid']]
                self._pb_delete_plan_clicked()
            self._skip_flag = False
            self._was_skipped = True
            try:
                self.model.queue_start()
            except Exception as ex:
                print(f"Exception: {ex}")

        
        elif manager_state == 'executing_queue':
            if running_uid and not running_uid == self._running_uid:
                self._running_uid = running_uid
                item_queue = []
                item_queue = copy.deepcopy(self._plan_queue_items)
                if not self.running_item in item_queue:
                    item_queue.insert(0, self.running_item)
                self.signal_plan_queue_changed.emit(item_queue, [])
            


        if isinstance(event, bool):
            is_connected = event
        else:
            is_connected = bool(event.is_connected)

        if not self._table.isEnabled() and (manager_state == 'idle' or manager_state == 'paused') and not self._skip_flag:
            self._table.setEnabled(True)
        if self._stop_flag and manager_state == 'paused':
            try:
                self.model.re_stop()
            except Exception as ex:
                print(f"Exception: {ex}")
            finally:
                self._stop_flag = False
        

        self._update_widgets(is_connected)

    def _update_button_states(self):
        is_connected = bool(self.model.re_manager_connected)
        status = self.model.re_manager_status
        loop_mode_on = status["plan_queue_mode"]["loop"] if status else False
        mon = self._monitor_mode
        n_items = self._n_table_items
        is_sel = len(self.model._selected_queue_item_uids) > 0

        worker_exists = status.get("worker_environment_exists", False)
        running_item_uid = status.get("running_item_uid", None)
        queue_stop_pending = status.get("queue_stop_pending", False)
        manager_state = status.get("manager_state", None)

        self._start_btn.setEnabled(is_connected and worker_exists and (bool(running_item_uid) or n_items))
        if bool(running_item_uid) and not (manager_state == "paused"):
            self._start_btn.setIcon(self._pause_icon)
            self._start_btn.setToolTip('Pause Queue')
        else:
            self._start_btn.setIcon(self._start_icon)
            self._start_btn.setToolTip('Start Queue')
        self._stop_btn.setEnabled(is_connected and worker_exists and bool(running_item_uid))
        self._stop_btn.setChecked(queue_stop_pending)

        self._skip_btn.setEnabled(is_connected and worker_exists and bool(running_item_uid) and n_items>1)
        self._skip_btn.setChecked(queue_stop_pending)

        self._loop_btn.setEnabled(is_connected and not mon)
        self._loop_btn.setChecked(loop_mode_on)
        _style_tag = "background-color: lightgreen" if loop_mode_on else ""
        self._loop_btn.setStyleSheet(_style_tag)
        self._clear_btn.setEnabled(is_connected and not mon and n_items)
        self._del_btn.setEnabled(is_connected and not mon and is_sel)
        self._batch_btn.setEnabled(is_connected and not mon and is_sel)
        self._edit_btn.setEnabled(is_connected and not mon and is_sel)
        self._clone_btn.setEnabled(is_connected and not mon and is_sel)

        if self._flag_snapshot_pause:
            self._stop_btn.setIcon(self._reload_icon)
            self._stop_btn.setToolTip('Reload Snapshot')
            self._stop_btn.setEnabled(True)
        else:
            self._stop_btn.setIcon(self._stop_icon)
            self._stop_btn.setToolTip('Stop Queue')

            

    def on_vertical_scrollbar_value_changed(self, value):
        max = self._table.verticalScrollBar().maximum()
        self._table_scrolled_to_bottom = value == max

    def on_vertical_scrollbar_range_changed(self, min, max):
        if self._table_scrolled_to_bottom:
            self._table.verticalScrollBar().setValue(max)

    def on_table_drop_event(self, row, col):
        # If the selected queue item is not in the table anymore (e.g. sent to execution),
        #   then ignore the drop event, since the item can not be moved.
        if self.model.selected_queue_item_uids:
            uid_ref_item = self.model.queue_item_pos_to_uid(row)
            try:
                self.model.queue_items_move_in_place_of(uid_ref_item)
            except Exception as ex:
                print(f"Exception: {ex}")

        self._update_button_states()

    def on_table_scroll_event(self, scroll_direction):
        v = self._table.verticalScrollBar().value()
        v_max = self._table.verticalScrollBar().maximum()
        if scroll_direction == "up" and v > 0:
            v_new = v - 1
        elif scroll_direction == "down" and v < v_max:
            v_new = v + 1
        else:
            v_new = v
        if v != v_new:
            self._table.verticalScrollBar().setValue(v_new)

    def on_plan_queue_changed(self, event):
        plan_queue_items = event.plan_queue_items
        selected_item_uids = event.selected_item_uids
        self.signal_plan_queue_changed.emit(plan_queue_items, selected_item_uids)

    @Slot(object, object)
    def slot_plan_queue_changed(self, plan_queue_items, selected_item_uids):
        # Check if the vertical scroll bar is scrolled to the bottom. Ignore the case
        #   when 'scroll_value==0': if the top plan is visible, it should remain visible
        #   even if additional plans are added to the queue.
        self._block_table_selection_processing = True

        # Create local copy of the plan queue items for operations performed locally
        #   within the widget without involving the model.
        self._plan_queue_items = copy.deepcopy(plan_queue_items)

        scroll_value = self._table.verticalScrollBar().value()
        scroll_maximum = self._table.verticalScrollBar().maximum()
        self._table_scrolled_to_bottom = scroll_value and (scroll_value == scroll_maximum)

        self._table.clearContents()
        self._table.setRowCount(len(plan_queue_items))

        if len(plan_queue_items):
            resize_mode = QHeaderView.ResizeToContents
        else:
            # Empty table, stretch the header
            resize_mode = QHeaderView.Stretch
        self._table.horizontalHeader().setSectionResizeMode(resize_mode)

        is_first = True

        for nr, item in enumerate(plan_queue_items):
            for nc, col_name in enumerate(self._table_column_labels):
                try:
                    value = self.model.get_item_value_for_label(item=item, label=col_name)
                except KeyError:
                    value = ""
                table_item = QTableWidgetItem(value)
                table_item.setFlags(table_item.flags() & ~Qt.ItemIsEditable)

                if self._flag_queue_running and is_first:
                    if self._manager_state == 'paused':
                        table_item.setBackground(QColor(169, 169, 169))
                        table_item.setFlags(table_item.flags() & ~Qt.ItemIsSelectable)
                        self._id_to_delete = item['item_uid']
                    else:
                        table_item.setBackground(QColor(51, 255, 51))

                self._table.setItem(nr, nc, table_item)
            if is_first:
                is_first = False

        # Update the number of table items
        self._n_table_items = len(plan_queue_items)

        # Advance scrollbar if the table is scrolled all the way down.
        if self._table_scrolled_to_bottom:
            scroll_maximum_new = self._table.verticalScrollBar().maximum()
            self._table.verticalScrollBar().setValue(scroll_maximum_new)

        self._block_table_selection_processing = False

        self.slot_change_selection(selected_item_uids)
        self._update_button_states()
    
    def set_snapshot(self):
        self.running_item = None
        self._flag_snapshot_pause = False
        self._flag_last_item_stopped = False
        self._was_skipped = False
        self._queue_snapshot = copy.deepcopy(self._plan_queue_items)

    def soft_reset_snapshot(self):
        self._flag_last_item_stopped = False
        running_item = copy.deepcopy(self.running_item)
        self.model.queue_item_add(item=self.running_item, params={"pos": "front"})
        self._table.setSelectionMode(4)
        self._table.setEnabled(True)
        self._update_button_states()
    
    def reset_snapshot(self):
        if not self._queue_snapshot:
            self._queue_snapshot = None
            self._flag_snapshot_pause = False
            self._flag_last_item_stopped = False
            self._was_skipped = False
            self.running_item = None
            self.model.queue_clear()
            self._table.setSelectionMode(4)
            self._table.setEnabled(True)
            self._update_button_states()
        else:
            snapshot = copy.deepcopy(self._queue_snapshot)
            self._queue_snapshot = None
            self._flag_snapshot_pause = False
            self._flag_last_item_stopped = False
            self._was_skipped = False
            self.running_item = None
            self.model.queue_clear()
            self.model.queue_item_add_batch(items=snapshot)
            self._table.setSelectionMode(4)
            self._table.setEnabled(True)
            self._update_button_states()


    def on_item_selection_changed(self):
        """
        The handler for ``item_selection_changed`` signal emitted by QTableWidget
        """
        if self._block_table_selection_processing:
            return

        sel_rows = self._table.selectionModel().selectedRows()
        try:
            if len(sel_rows) >= 1:
                selected_item_pos = [_.row() for _ in sel_rows]
                selected_item_uids = [self.model.queue_item_pos_to_uid(_) for _ in selected_item_pos]
                self.model.selected_queue_item_uids = selected_item_uids
                self._selected_items_pos = selected_item_pos
            else:
                raise Exception()
        except Exception:
            self.model.selected_queue_item_uids = []
            self._selected_items_pos = []

    def on_queue_item_selection_changed(self, event):
        """
        The handler for the event generated by the model
        """
        selected_item_uids = event.selected_item_uids
        self.signal_update_selection.emit(selected_item_uids)

    @Slot(object)
    def slot_change_selection(self, selected_item_uids):

        rows = [self.model.queue_item_uid_to_pos(_) for _ in selected_item_uids]

        # Keep horizontal scroll value while the selection is changed (more consistent behavior)
        scroll_value = self._table.horizontalScrollBar().value()
        if not rows:
            self._table.clearSelection()
            self._selected_items_pos = []
        else:
            self._block_table_selection_processing = True
            self._table.clearSelection()
            for row in rows:
                if self._table.currentRow() not in rows:
                    self._table.setCurrentCell(rows[-1], 0)
                for col in range(self._table.columnCount()):
                    item = self._table.item(row, col)
                    if item:
                        item.setSelected(True)
                    else:
                        pass
                        #print(f"Plan Queue Table: attempting to select non-existing item: row={row} col={col}")

            row_visible = rows[-1]
            item_visible = self._table.item(row_visible, 0)
            self._table.scrollToItem(item_visible, QAbstractItemView.EnsureVisible)
            self._block_table_selection_processing = False

            self._selected_items_pos = rows

        self._table.horizontalScrollBar().setValue(scroll_value)

        self.model.selected_queue_item_uids = selected_item_uids
        self._update_button_states()

    def _on_table_cell_double_clicked(self, n_row, n_col):
        """
        Double-clicking of an item of the table widget opens the item in Plan Editor.
        """
        # We use local copy of the queue here
        try:
            queue_item = self._plan_queue_items[n_row]
        except IndexError:
            queue_item = None
        registered_editors = self.registered_item_editors

        # Do nothing if item is not found or there are no registered editors
        if not queue_item or not registered_editors:
            return

        item_accepted = False
        for editor_activator in registered_editors:
            try:
                item_accepted = editor_activator(queue_item)
            except Exception:
                print(f"Editor failed to start for the item {queue_item['name']}")

            if item_accepted:
                break

        if not item_accepted:
            print(f"Item {queue_item['name']!r} was rejected by all registered editors")

    def _pb_clear_queue_clicked(self):
        dialog = QDialog(self)
        dialog.setWindowTitle("Clear Queue")
        
        layout = QVBoxLayout()
        label = QLabel("Do you really want to clear the queue?")
        layout.addWidget(label)
        
        button_box = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        )
        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)
        layout.addWidget(button_box)
        
        dialog.setLayout(layout)
        
        if dialog.exec_() == QDialog.Accepted:
            try:
                self.model.queue_clear()
            except Exception as ex:
                print(f"Exception: {ex}")

    def _pb_loop_on_clicked(self):
        loop_enable = self._loop_btn.isChecked()
        if loop_enable:
            self._loop_btn.setStyleSheet("background-color: lightgreen")
        else:
            self._loop_btn.setStyleSheet("")
        try:
            self.model.queue_mode_loop_enable(loop_enable)
        except Exception as ex:
            print(f"Exception: {ex}")
        
    def _pb_start_clicked(self):
        status = self.model.re_manager_status
        manager_state = status.get("manager_state", None)

        if manager_state == 'idle':
            try:
                self.set_snapshot()
                self.model.queue_start()
            except Exception as ex:
                print(f"Exception: {ex}")
        elif manager_state == 'paused':
            try:
                self.model.re_resume()
            except Exception as ex:
                print(f"Exception: {ex}")
        elif manager_state == 'executing_queue':
            try:
                self.model.re_pause(option="deferred")
            except Exception as ex:
                print(f"Exception: {ex}")
        else:
            print('Weird manager state: ', manager_state)
    
    def _pb_stop_clicked(self):
        status = self.model.re_manager_status
        manager_state = status.get("manager_state", None)

        if self._flag_snapshot_pause:
            self._flag_snapshot_pause = False
            self.reset_snapshot()

        elif not manager_state == 'paused':
            try:
                self.model.re_pause(option="deferred")
                self._stop_flag = True  
            except Exception as ex:
                print(f"Exception: {ex}")
        else:
            try:
                self.model.re_stop()
            except Exception as ex:
                print(f"Exception: {ex}")  
        self.signal_update_widgets.emit(True)

    def _pb_skip_clicked(self):
        status = self.model.re_manager_status
        manager_state = status.get("manager_state", None)
        self._table.setEnabled(False)
        if manager_state == 'executing_queue':
            try:
                self.model.re_pause(option="deferred")
                self._skip_flag = True
            except Exception as ex:
                print(f"Exception: {ex}")
        elif manager_state == 'paused':
            self._skip_flag = True
        self.signal_update_widgets.emit(True)

    def _pb_clone_clicked(self):
        try:
            self.model.queue_item_copy_to_queue()
        except Exception as ex:
            print(f"Exception: {ex}")

    def _pb_delete_plan_clicked(self):
        try:
            self.model.queue_items_remove()
        except Exception as ex:
            print(f"Exception: {ex}") 

    def _pb_edit_clicked(self):
        sel_item_uids = self.model.selected_queue_item_uids
        if len(sel_item_uids) == 1:
            sel_item_uid = sel_item_uids[0]
            sel_item = self.model.queue_item_by_uid(sel_item_uid)  # Returns deep copy
            self.model.events.queue_item_edit(item=sel_item)
            self._pb_delete_plan_clicked()

    def _pb_batch_clicked(self):
        sel_item_uids = self.model.selected_queue_item_uids
        items = []
        for item_uid in sel_item_uids:
            x = self.model.queue_item_by_uid(item_uid)
            x['args'] = []
            items.append(x)

        name, _ = QFileDialog.getSaveFileName(self, 'Save File', self.BATCH_DIR)
        
        if not name:
            return

        try:
            things = create_excel_file_from_plan_list(self.BATCH_DIR, plan_list=items, ss_filename=name, ss_ext='.csv')
        except AssertionError as e:
            pass

    def _pb_load_clicked(self):
        name, _ = QFileDialog.getOpenFileName(self, 'Open File', self.BATCH_DIR, '*.csv')
        
        if not name:
            return

        with open(name) as a_file:
            items = spreadsheet_to_plan_list(spreadsheet_file=a_file, file_name=name)
            self.model.queue_item_add_batch(items=items)

        