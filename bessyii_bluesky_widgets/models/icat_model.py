from bluesky_live.event import EmitterGroup, Event

import Levenshtein as lev
import requests
import logging
import json
import time

import icat.config
import icat.client
from icat.query import Query
icat.config.defaultsection = 'default'

from bluesky_widgets.qt.threading import GeneratorWorker


class ICatModel:

    def __init__(self):
        self._connected = None
        self._user_name = None
        self._user_profile = None
        self._password = None
        self._investigations = None
        self._selection = None
        self._icat_header = None
        self._client = None
        self._refresh_worker = None

        self.events = EmitterGroup(
            source=self,
            status_changed=Event,
            investigation_changed=Event,
        )
        
    @property    
    def connected(self):
        return self._connected

    @property
    def investigations(self):
        return self._investigations
    
    @property
    def full_name(self):
        return self._full_name
    
    @property
    def username(self):
        return self._username
    
    @property
    def selected_title(self):
        if self._selection:
            return self._selection['title']
        return None
    
    @property
    def selected_name(self):
        if self._selection:
            return self._selection['name']
        return None

    @property
    def selected_id(self):
        if self._selection:
            return self._selection['id']
        return None

    @property
    def icat_header(self):
        return self._icat_header

    @property
    def md_dict(self):
        if self._connected:
            _dict = {
                'investigationid': self.selected_id,
                'name_of_investigation': self.selected_name,
                'title_of_investigation': self.selected_title,
                'user_name': self.full_name,
                'user_profile': self.username,
            }
            _dict.update(self.icat_header)
        else:
            _dict = {}
        return _dict
    
    def logout(self):
        self._connected = False
        self._investigations = None
        self._full_name = None
        self._selection = None
        self._username = None
        self._password = None
        self._icat_header = None
        self.events.status_changed(status='default')
        self.events.investigation_changed(status='default')
        try:
            self._client.logout()
        except AttributeError as e:
            pass
        self._client = None
        try:
            self._refresh_worker.quit()
        except RuntimeError as e:
            pass
        except AttributeError as e:
            pass
        self._refresh_worker = None
    
    def select_investigation(self, selection):
        if selection:
            assert(all(key in selection.keys() for key in ['id', 'name', 'title']))
            self._selection = selection
            self.events.investigation_changed(status='selection')
        elif self._selection is None:
            self.logout()

    def requestInvestigationNames(self, username, password):

        self._icat_header = {'icat': {'idsurl': 'https://icat.helmholtz-berlin.de',
            'sessionId': 'adfba0ef-878e-418f-8ab5-7482750fa23d',
            'url': 'https://icat.helmholtz-berlin.de'}}
        self._connected = True
        self._full_name = "Max Mustermann"
        self._investigations = {'CPMU17_EMIL Beamline Commissioning 2021/2022':{'id': '7923', 'name': 'misc:21-000008'},
        'METRIXS Commissioning 2022': {'id': '7933', 'name': 'misc:22-000016'},
        'PM4 Beamline Commissioning 2021/2022': {'id': '7924', 'name': 'misc:21-000009'},
        'Test: ingest for SISSY': {'id': '7645', 'name': 'gate:202-00003-1.1-P'},
        'UE48 Beamline Commissioning 2021': {'id': '7874', 'name': 'misc:21-000005'},
        'UE52_SGM Commissioning 2022': {'id': '7935', 'name': 'misc:22-000017'}}
        self._username = username
        self._password = password
        self.events.status_changed(status='connected')
        return

        """
        
        A function which authenticates against the ICAT and get's investigations. 
        Aims to be used inside other functions in both GUI and IPython environment. 
        Does not ask for input or present anything in shell, that is for other calling functions to perform
        
        
        Arguments
        ---------
        
        username: string
        password: string
        
        Returns
        ---------
        
        icat_dict: Dict 
            Icat sessionid, icat url, icat investigation id and name.
        
        example: 
        
        {'icat': {'idsurl': 'https://icat.helmholtz-berlin.de',
            'sessionId': adfba0ef-878e-418f-8ab5-7482750fa23d,
            'url': 'https://icat.helmholtz-berlin.de'}}
            
        investigations: Dict
            with keys of the investigation title and items of another dict with the name and id of that title
        
        example: 
        
        {'CPMU17_EMIL Beamline Commissioning 2021/2022':{'id': '7923', 'name': 'misc:21-000008'},
        'METRIXS Commissioning 2022': {'id': '7933', 'name': 'misc:22-000016'},
        'PM4 Beamline Commissioning 2021/2022': {'id': '7924', 'name': 'misc:21-000009'},
        'Test: ingest for SISSY': {'id': '7645', 'name': 'gate:202-00003-1.1-P'},
        'UE48 Beamline Commissioning 2021': {'id': '7874', 'name': 'misc:21-000005'},
        'UE52_SGM Commissioning 2022': {'id': '7935', 'name': 'misc:22-000017'}}        
        """

        credentials = {"username": username,"password": password}
        auth = 'hzbrex'
        url = 'https://icat.helmholtz-berlin.de'
                    
        try:
            self._client = icat.client.Client(url,idsurl=url)
            self._client.login(auth, credentials)

            try:
                query = Query(self._client, "Investigation", conditions={
                        "investigationUsers.user.name": "= :user"
                        }, aggregate="DISTINCT",includes=["investigationUsers.user"])
                query_fname = Query(self._client, "User", conditions={"name": "= '" + self._client.getUserName() + "'"})
                full_name = str(self._client.search(query_fname)[0].fullName)
                investigations_query_response = self._client.search(query)
                investigations = {}
                for item in investigations_query_response:
                    investigations[str(item.title)] = {'name': str(item.name), 'id': str(item.id)}
                                            

                self._icat_header = {"icat":{"url":str(url),"idsurl":str(url),"sessionId":str(self._client.sessionId)}}
                self._connected = True
                self._full_name = full_name
                self._investigations = investigations
                self._username = username
                self._password = password
                self._refresh_worker = GeneratorWorker(self._client_refresh_loop)
                self._refresh_worker.start()
                self.events.status_changed(status='connected')
            except Exception as e:
                print('Error while fetching Investigations.')
                raise e
            self._icat_header = {"icat":{"url":str(url),"idsurl":(url),"sessionId":(self._client.sessionId)}}
        except Exception as e:
            print('Error while generating SessionID.')
            raise e
        print(2)

    def refresh_client(self):
        if self._connected:
            try:
                self._client.autoRefresh()
            except AttributeError as e:
                self.logout()
        else:
            print('Not connected!')
    
    def _client_refresh_loop(self):
        while True:
            self.refresh_client()
            time.sleep(1)
            yield
        
            
        