from bluesky_live.event import EmitterGroup, Event

import Levenshtein as lev
import requests
import logging
import json


class ElogModel:

    def __init__(self):
        self._connected = None
        self._investigations = None
        self._full_name = None
        self._username = None
        self._password = None
        self._selection = None

        self.events = EmitterGroup(
            source=self,
            status_changed=Event,
            investigation_changed=Event,
        )
    
    @property
    def connected(self):
        return self._connected

    @property
    def investigations(self):
        return self._investigations
    
    @property
    def full_name(self):
        return self._full_name
    
    @property
    def username(self):
        return self._username
    
    @property
    def selected_title(self):
        if self._selection:
            return self._selection['title']
        return None
    
    @property
    def selected_name(self):
        if self._selection:
            return self._selection['name']
        return None

    @property
    def selected_id(self):
        if self._selection:
            return self._selection['id']
        return None

    @property
    def md_dict(self):
        if self.connected:
            _dict = {
                'ElogID': str(self.selected_id),
                'Name of Investigation': str(self.selected_name),
                'Title of Investigation': str(self.selected_title),
                'Author': str(self.full_name),
                'User': str(self.username),
            }
        else:
            _dict = {}
        return _dict
        
    
    def logout(self):
        self._connected = False
        self._investigations = None
        self._full_name = None
        self._selection = None
        self._username = None
        self._password = None
        self.events.status_changed(status='default')
        self.events.investigation_changed(status='default')
    
    def select_investigation(self, selection):
        if selection:
            assert(all(key in selection.keys() for key in ['id', 'name', 'title']))
            self._selection = selection
            self.events.investigation_changed(status='selection')
        elif self._selection is None:
            self.logout()

    def requestInvestigationNames(self, username, password):
        #based on https://gitlab.helmholtz-berlin.de/bessyII/bluesky/bessyii/-/blob/master/bessyii/eLog.py
        #tries to login to elog and returns a dict with key=Name of investigation and value=investigationID

        self.events.status_changed(status='connecting')
        connect_dict = self.getSessionID(username,password)

        try:
            session_id = connect_dict['session_id']
            full_name = connect_dict['full_name']
            connected = connect_dict['connected']
        except KeyError as e:
            self.events.status_changed('failed')
            session_id = None
        
        if session_id == None:
            self.events.status_changed(status='failed')
        else:
            url = 'https://icat.helmholtz-berlin.de/icatplus/catalogue/'+session_id+'/investigation'
            header = {'Content-Type': 'application/json'}
            proxies = {"http": "http://www.bessy.de:3128", "https": "http://www.bessy.de:3128"}
            
            response = requests.get(url, headers=header, proxies=proxies)
            code = response.status_code
            if code == 200:
                response_data = json.loads(response.text)
                investigations = {}

                for item in response_data:
                    investigations[str(item['title'])] = {'name': str(item['name']), 'id': str(item['id'])}
                
                self._full_name = full_name,
                self._investigations = investigations
                self._username = username
                self._password = password
                self._connected = True,

                self.events.status_changed(status='connected')

            else:
                self.events.status_changed(status='failed')
    
    def getSessionID(self, username, password):
        #based on https://gitlab.helmholtz-berlin.de/bessyII/bluesky/bessyii/-/blob/master/bessyii/eLog.py
        proxies = {"http": "http://www.bessy.de:3128", "https": "http://www.bessy.de:3128"}
        header = {'Content-Type': 'application/json'}
        data = {"plugin":"hzbrex","username":username,"password":password}
        session_id = None
        full_name = None
        response = requests.post('https://icat.helmholtz-berlin.de/icatplus/session', headers=header,data=json.dumps(data), proxies=proxies)
        code = response.status_code
        if code == 200:
            response_data = json.loads(response.text)
            session_id = response_data['sessionId']
            full_name = response_data['fullName']
            if session_id and full_name:
                return {
                    'connected': True,
                    'session_id': session_id,
                    'full_name': full_name,
                    }
            else:
                return {
                    'connected': False,
                    'session_id': None,
                    'full_name': None,
                    }
        else:
            return {
                'connected': False,
                'session_id': None,
                'full_name': None,
                }