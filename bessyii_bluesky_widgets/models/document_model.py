from bluesky_live.event import EmitterGroup, Event
from bluesky_widgets.qt.zmq_dispatcher import RemoteDispatcher as QtRemoteDispatcher

from bluesky_widgets.qt.threading import create_worker
import queue

class DocumentModel:

    def __init__(self, ip='localhost:5578'):
        self.events = EmitterGroup(
            source=self,
            status_changed = Event,
            scan_status = Event,
            new_data = Event,
        )
        self.ip = ip
        self.start_msg = None
        self.zmq_dispatcher = QtRemoteDispatcher(self.ip)
        self._dispatch_worker = create_worker(self.zmq_dispatcher.start)
        self._dispatch_worker.start()
        self.add_callback(self.msg_callback)
        self.event_streams = {}

    def add_callback(self, callback):
        self.zmq_dispatcher.subscribe(callback)

    def close(self):
        self.zmq_dispatcher.stop()
        
    def msg_callback(self, doc_type, msg):
        if doc_type == 'start':
            self.events.scan_status(
                status='start',
                data = msg,
            )
        elif doc_type == 'stop':
            self.event_streams.clear()
            self.events.scan_status(
                status='stop',
                data = msg,
            )
        elif doc_type == 'event':
            self.events.new_data(
                data = msg,
                start_msg = self.start_msg,
            )
        elif doc_type == 'descriptor':
            self.event_streams[msg['name']] = msg['data_keys']
            

        
    